
(in-package :cl-user)
(defpackage :kononoha_mikoto-asd
  (:use :cl :asdf))
(in-package :kononoha_mikoto-asd)

(defsystem "kotonoha"
  :version "0.1.0"
  :author "i-makinori"
  :license ""
  :depends-on (:mcclim)
  :components
  ((:module "kotonoha"
    :components
    ((:file "package")
     (:file "life")
     (:file "kanputer")
     (:file "functions")
     (:file "world")
     (:file "observe"))))
  :description "")

