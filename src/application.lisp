(in-package :kotonoha_mikoto)

;; app-main

(defun app-main (&optional (simulation-type :recents))
  (setf *current-simulation* simulation-type)
  (run-mcclim-application)
  )

;; profile

(defun profile-app-main ()
  (sb-profile:unprofile)
  ;; (sb-profile:profile "KOTONOHA_MIKOTO" "CLIM" "CL-USER")
  (sb-profile:profile "KOTONOHA_MIKOTO" "CL-USER")
  (time 
   ;;
   (kotonoha_mikoto::app-main)
   ;;
   )
  (sb-profile:report :print-no-call-list nil)
  (sb-profile:unprofile)
  )