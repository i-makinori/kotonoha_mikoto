
(in-package :kotonoha_mikoto)

(defparameter *sample-array*
  (make-array (list 5 6)
              :initial-element 0
              ))

(defun upto (from until)
  (labels 
      ((aux (num list)
         (cond ((> num until) (reverse list))
               (t (aux (1+ num) (cons num list))))))
    (aux from '())))

(defun dimension-counter (dimensions)
  (labels 
      ((dimension-list (dim list)
         ;;(print dim)
         (cond ((null dim) (list list))
               (t 
                (reduce #'(lambda (n l)
                            (append (dimension-list (cdr dim) 
                                                    (cons n list)) 
                                    l))
                        (upto 0 (1- (car dim)))
                        :initial-value '() :from-end t)))))
    (dimension-list dimensions '())))


(defun loop-array (array array->&dims->proc)
  (labels
      ((aux (arr coords)
         (format t "~A, ~A ~%" coords arr)
         (cond ((arrayp arr)
                (loop for iter from 0 below (array-dimension arr 0)
                      collect (aux (aref arr iter) (cons iter coords))))
               (t
                (apply array->&dims->proc (cons array (reverse coords))))
               )))
    (aux array '())))



(defun iter-matrix (matrix y->x->matrix->proc)
  (make-array (array-dimensions matrix)
              :initial-contents
              (destructuring-bind (dim-y dim-x) (array-dimensions matrix)
                (loop 
                  for y from 0 below dim-y
                  collect (loop 
                            for x from 0 below dim-x
                            collect (funcall y->x->matrix->proc y x matrix
                            ))))))

(defun map-matrix (function matrix)
  (iter-matrix 
   matrix
   #'(lambda (y x m)
       (funcall function (aref m y x)))))

(defun convolution-2d-modulo (function radius matrix)
  (destructuring-bind (dim-y dim-x) (array-dimensions matrix)
    (iter-matrix
     matrix
     #'(lambda (center_y center_x the_m)
         (funcall
          function
          (loop for delta_y from (- radius) upto radius
                collect (loop for delta_x from (- radius) upto radius
                              collect (aref the_m
                                            (mod (+ center_y delta_y) dim-y)
                                            (mod (+ center_x delta_x) dim-x)
                                            ))))))))