
(in-package :kotonoha_mikoto)


;; uniform grid

;;(defparameter *num-grid* 32)

(defun num-cell-of-space (num-grid)
  (* num-grid num-grid))


;; liner uniformed space 

(defun index-index-to-liner-index (y x num-grid)
  (+ (* y num-grid) x))

;; index

(defun index-of-dimension (point length num-grid)
  ;;(floor (* num-grid (/ (torus-coordinate-dimension point length)
  ;;length))))
  (min (1- num-grid)
       (max 0 (floor (* num-grid (/ point length))))
  ))

(defun index-index-to-index-region (less-index larger-index num-grid)
  (if (<= less-index larger-index)
      (upto less-index larger-index)
      (append (upto less-index (- num-grid 1)) (upto 0 larger-index))))

(defun list-list-pattern-zip-list (list-a list-b)
  (apply #'append
         (mapcar #'(lambda (a)
                     (mapcar #'(lambda (b) (cons a b))
                             list-b))
                 list-a)))

  
(defun space-liner-index-list
    (top-index bottom-index left-index right-index num-grid)
  (let ((vertical-index-list
          (index-index-to-index-region top-index bottom-index
                                       num-grid))
        (horizonal-index-list
          (index-index-to-index-region left-index right-index
                                       num-grid)))
    (mapcar #'(lambda (yx)
                (index-index-to-liner-index (car yx) (cdr yx) num-grid))
            (list-list-pattern-zip-list vertical-index-list
                                        horizonal-index-list))))


;; aabb

(defun space-liner-index-list-of-aabb (aabb space-scale num-grid)
  (let ((top-index-of-dimension
          (index-of-dimension (aabb-top aabb) (space-scale-height space-scale) num-grid))
        (bottom-index-of-dimension
          (index-of-dimension (aabb-bottom aabb) (space-scale-height space-scale) num-grid))
        (left-index-of-dimension
          (index-of-dimension (aabb-left aabb) (space-scale-width space-scale) num-grid))
        (right-index-of-dimension
          (index-of-dimension (aabb-right aabb) (space-scale-width space-scale) num-grid)))
    (space-liner-index-list top-index-of-dimension bottom-index-of-dimension
                            left-index-of-dimension right-index-of-dimension
                            num-grid)))



;; substance

(defun substance-uniformd-grid-indexes (substance space-scale num-grid)
  (space-liner-index-list-of-aabb
   (substance-aabb substance)
   space-scale num-grid))


;; sequence

(defun liner-uniformed-grid-2d-space (num-grid)
  (make-array (num-cell-of-space num-grid)
              :initial-element nil))

#|
(defparameter *uniformed-grid-sequence*
  (liner-uniformed-grid-2d-space *num-grid*))
|#

#|
(defun push-object-to-sequence! (sequence index-list object)
  (loop 
    for i in index-list
    do (if (find object (aref sequence i) :test #'eq)
           nil
           (setf (aref sequence i)
                 (cons object (aref sequence i)))))
    sequence)


(defun remove-object-from-sequence! (sequence index-list object)
  (loop
    for i in index-list
    do (setf (aref sequence i)
             (remove object (aref sequence i)
                     :test #'eq)))
  sequence)


(defun object-move-sequence!
    (sequence! indexes-ago indexes-next object)
  (unless (equal indexes-ago indexes-next)
    (remove-object-from-sequence! sequence! indexes-ago object)
    (push-object-to-sequence! sequence! indexes-next object))
  sequence!)
|#


;; sequence for substance

(defun push-substance-to-uniformed-grid-sequence! (sequence index-list substance)
  (loop 
    for i in index-list
    do (if (find substance (aref sequence i)
                 :test #'substance-key-equal)
           nil
           (setf (aref sequence i)
                 (cons substance (aref sequence i)))))
    sequence)

(defun remove-substance-from-uniformed-gird-sequence! (sequence index-list substance)
  (loop
    for i in index-list
    do (setf (aref sequence i)
             (remove substance (aref sequence i)
                     :test #'substance-key-equal)))
  sequence)


(defun substance-update-uniformed-grid-sequence!
    (sequence! ago-indexes updated-substance space-scale num-grid)
  (let ((next-indexes
          (substance-uniformd-grid-indexes
           updated-substance space-scale num-grid)))
    ;; (unless (equal next-indexes ago-indexes)
    ;; all of substance pushed is remained in sequence if pointer is different
    ;; when such as renew structure (?)
    (remove-substance-from-uniformed-gird-sequence! sequence! ago-indexes updated-substance)
    (push-substance-to-uniformed-grid-sequence! sequence! next-indexes updated-substance))
  sequence!)

(defun substance-update-uniformed-grid-sequence!-for-destructed-update-substance!
    (sequence! ago-indexes updated-substance space-scale num-grid)
  (let ((next-indexes
          (substance-uniformd-grid-indexes
           updated-substance space-scale num-grid)))
    (unless (equal next-indexes ago-indexes) 
      ;; update-function which does destructive assignment can use index-equal filter 
      ;; for registing to sequence!
      (remove-substance-from-uniformed-gird-sequence! sequence! ago-indexes updated-substance)
      (push-substance-to-uniformed-grid-sequence! sequence! next-indexes updated-substance)))
  sequence!)




;; collision-detection


(defparameter *num-collision-detection* 0)


(defmacro something--uniformed-grid--collision-detection
    (collision-detection uniformed-grid-indexes)
  `(remove 
      nil
      (mapcar 
       #'(lambda (sub)
           (incf *num-collision-detection*)
           ;; (substance-substance-collision-detection substance sub space-scale)))
           ,collision-detection
           )
       (remove-duplicates 
        (apply #'append
               (mapcar #'(lambda (i) (aref uniformed-grid-sequence i))
                       ;; (substance-uniformd-grid-indexes substance space-scale num-grid)))
                       ,uniformed-grid-indexes
                       ))
        :test #'substance-key-equal))))


(defun substance--uniformed-grid--collision-detection
    (substance uniformed-grid-sequence space-scale num-grid)
  (labels ((the-detection (sub)
             (if (substance-key-equal sub substance)
                 nil
                 (substance-substance-collision-detection substance sub space-scale))))
    (remove substance
            (something--uniformed-grid--collision-detection
             ;;(substance-substance-collision-detection substance sub space-scale)
             (the-detection sub)
             (substance-uniformd-grid-indexes substance space-scale num-grid))
            :test #'substance-key-equal)))


(defun aabb--uniformed-grid--collision-detection
    (aabb-is-not-substance uniformed-grid-sequence space-scale num-grid)
  (something--uniformed-grid--collision-detection
   (aabb-substance-collision-detection aabb-is-not-substance sub space-scale)
   (space-liner-index-list-of-aabb aabb-is-not-substance space-scale num-grid)))


(defun what-there-are-is-what-is-informed (coordinated-there coordinated-may-colls)
  coordinated-there
  coordinated-may-colls)

(defun aabb--uniformed-grid--collision-detection--omitted
    (aabb-is-not-substance uniformed-grid-sequence space-scale num-grid)
  (something--uniformed-grid--collision-detection
   ;; (aabb-substance-collision-detection-omitted aabb-is-not-substance sub space-scale)
   (what-there-are-is-what-is-informed aabb-is-not-substance sub)
   (space-liner-index-list-of-aabb aabb-is-not-substance space-scale num-grid)))


