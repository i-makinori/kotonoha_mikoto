
(in-package :kotonoha_mikoto)



;;; physics

(defun vec (dy dx)
  "vector dimension_m dimension_n"
  (vector dy dx))

(defun vec-dy (vec)
  "vector dimension_y"
  (aref vec 0))

(defun vec-dx (vec)
  "vector dimension_n"
  (aref vec 1))

(defun mapvec (function vec &rest more-vecs)
  (apply #'map (append (list 'vector function vec) more-vecs)))

(defun vec-add (v1 v2)
  "vector addision"
  (vec (+ (vec-dy v1) (vec-dy v2))
       (+ (vec-dx v1) (vec-dx v2))))


(defstruct (substance)
  (unique-key (gensym))
  ;; 質点[Dim_Y(t), Dim_X(t)]
  (coordinate (vector 0.00 0.00))
  ;; 方位 [Sita(t)]
  (direction 0.00)
  ;; 半径 [distance(t)]
  (radius 0.00)
  ;; 速度 [d-distance/d-time, d-sita/d-time]
  (speed (vector 0.00 0.00))
  ;; 加速度. 移動を目的とする戦術による変位量でもある｡ [dd-distance/dd-time, dd-sita/dd-time]
  (acceleration (vector 0.00 0.00))
  ;; 質量 [Real]
  (mass 0.00)
  ;;; 密度: 質量(t)/胞積(半径(t)) [g/(m^S)] : 力の歪み(場)として解釈する｡
  (density 0.00)
  ;; 衝突
  (collisioning nil)
)

(defun substance-key-equal (substance1 substance2)
  (eq (substance-unique-key substance1)
      (substance-unique-key substance2)))


;;; torus coordinate

#|
(defun torus-coordinate-dimension (point length)
  (mod point length)
)

(defun torus-coordinate (coordinate-vec space-scale)
  (mapvec #'mod
          coordinate-vec
          (list (space-scale-height space-scale) (space-scale-width space-scale))))

(defun torus-distance (p1 p2 length)
  (min (abs (- p1 p2))
       (abs (- length (- p1 p2)))))
|#

;;; circle

(defstruct circle-parameter
  (y 0) (x 0)
  (radius 0))


(defun substances-circle-parameter (substance)
  (make-circle-parameter 
   :y (vec-dy (substance-coordinate substance))
   :x (vec-dx (substance-coordinate substance))
   :radius  (substance-radius substance)))


;;; Axis Aligned Bounding Box

(defstruct aabb
  (left 0.0 :type single-float)
  (right 0.0 :type single-float)
  (top 0.0 :type single-float)
  (bottom 0.0 :type single-float))


(defun substance-aabb (substance)
  ;;(declare (optimize (speed 3) (safety 0) (debug 0))
  ;;(type substance substance))
  (let ((y (vec-dy (substance-coordinate substance)))
        (x (vec-dx (substance-coordinate substance)))
        (r (substance-radius substance)))
    (declare (type single-float y x r))
    (the aabb
         (make-aabb 
          :left (- x r)
          :right (+ x r)
          :top (- y r)
          :bottom (+ y r)))))



;;; collision detection


(defun circle-point-collision-detection (point-y point-x circle space-scale)
  space-scale
  (labels ((sq (n) (* n n)))
    (with-slots (y x radius) circle
      (< (+ (sq (- point-y y)) (sq (- point-x x)))
         (sq radius)))))


(defun circle-collision-detection (circle-parameter-a circle-parameter-b space-scale)
  space-scale
  (labels ((sq (n) (* n n)))
    (let* ((a-r (circle-parameter-radius circle-parameter-a))
           (a-y (circle-parameter-y circle-parameter-a))
           (a-x (circle-parameter-x circle-parameter-a))
           (b-r (circle-parameter-radius circle-parameter-b))
           (b-y (circle-parameter-y circle-parameter-b))
           (b-x (circle-parameter-x circle-parameter-b)))
      (<= (+ (sq (- a-y b-y)) (sq (- a-x b-x)))
      (+ (sq (+ a-r b-r))))))
      ;;(<= (+ (sq (torus-distance a-y b-y height)) (sq (torus-distance a-x b-x width)))
      ;;(sq (+ a-r b-r)))))
  )


(defun substance-substance-collision-detection (substance-there substance-may-collision space-scale)
  (if (and (aabb-aabb-collision-deteciton (substance-aabb substance-there)
                                          (substance-aabb substance-may-collision)
                                          space-scale)
           (circle-collision-detection
            (substances-circle-parameter substance-there)
            (substances-circle-parameter substance-may-collision)
            space-scale))
      substance-may-collision nil))



(defun aabb-point-collision-detection (point-y point-x aabb space-scale)
  space-scale
  (and (> (aabb-bottom aabb) point-y (aabb-top aabb))
       (> (aabb-right aabb) point-x (aabb-left aabb))))

(defun aabb-aabb-collision-deteciton (aabb-from aabb-to space-scale)
  space-scale
  (let ((top-0 (aabb-top aabb-from))
        (bottom-0 (aabb-bottom aabb-from))
        (left-0 (aabb-left aabb-from))
        (right-0 (aabb-right aabb-from))
        (top-1 (aabb-top aabb-to))
        (bottom-1 (aabb-bottom aabb-to))
        (left-1 (aabb-left aabb-to))
        (right-1 (aabb-right aabb-to)))
    (and (< top-0 bottom-1) (< top-1 bottom-0)
         (< left-0 right-1) (< left-1 right-0))))

(defun aabb-circle-collision-detection (aabb circle space-scale)
  (let ((circle-y (circle-parameter-y circle))
        (circle-x (circle-parameter-x circle))
        (circle-r (circle-parameter-radius circle))
        (aabb-top (aabb-top aabb))
        (aabb-bottom (aabb-bottom aabb))
        (aabb-left (aabb-left aabb))
        (aabb-right (aabb-right aabb)))
    (or
     (aabb-point-collision-detection
      circle-y circle-x
      (make-aabb :left (- aabb-left circle-r) :right (+ aabb-right circle-r)
                 :top aabb-top :bottom aabb-bottom)
      space-scale)
     (aabb-point-collision-detection
      circle-y circle-x
      (make-aabb :left aabb-left :right aabb-right
                 :top (- aabb-top circle-r) :bottom (+ aabb-bottom circle-r))
      space-scale)
     ;;
     (circle-point-collision-detection
      aabb-top aabb-left circle space-scale)
     (circle-point-collision-detection
      aabb-top aabb-right circle space-scale)
     (circle-point-collision-detection
      aabb-bottom aabb-left circle space-scale)
     (circle-point-collision-detection
      aabb-bottom aabb-right circle space-scale))))

(defun aabb-circle-collision-detection-omitted (aabb circle space-scale)
  (let ((circle-y (circle-parameter-y circle))
        (circle-x (circle-parameter-x circle))
        (circle-r (circle-parameter-radius circle))
        (aabb-top (aabb-top aabb))
        (aabb-bottom (aabb-bottom aabb))
        (aabb-left (aabb-left aabb))
        (aabb-right (aabb-right aabb)))
     (aabb-point-collision-detection
      circle-y circle-x
      (make-aabb :left (- aabb-left circle-r) :right (+ aabb-right circle-r)
                 :top (- aabb-top circle-r) :bottom (+ aabb-bottom circle-r))
      space-scale)))

(defun aabb-substance-collision-detection-omitted (aabb substance space-scale)
  (if (aabb-circle-collision-detection-omitted
       aabb (substances-circle-parameter substance) space-scale)
      substance
      nil))

(defun aabb-substance-collision-detection (aabb substance space-scale)
  (if (aabb-circle-collision-detection 
       aabb (substances-circle-parameter substance) space-scale)
      substance
      nil))

