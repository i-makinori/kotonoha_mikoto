
(in-package :kotonoha_mikoto)


;;; Camera

(defstruct camera
  (sekai-y 0.0) ;; "the center of camera are positioned in sekai's y"
  (sekai-x 0.0) ;; "the center of camera are positioned in sekai's x"
  (panel-width 0)
  (panel-height 0)
  (sekai-scale-unit--ratio--camera-scale-unit 1) ;; sekai 1 dot : camera 1 dot
)

(defun camera-aabb (camera)
  (with-slots (sekai-y sekai-x panel-width panel-height sekai-scale-unit--ratio--camera-scale-unit)
      camera
    (let ((zoom-ratio (identity (/ 1 sekai-scale-unit--ratio--camera-scale-unit))))
      (make-aabb
       :left (coerce (- sekai-x (* 1/2 zoom-ratio panel-width)) 'single-float)
       :right (coerce (+ sekai-x (* 1/2 zoom-ratio panel-width)) 'single-float)
       :top (coerce (- sekai-y (* 1/2 zoom-ratio panel-height)) 'single-float)
       :bottom (coerce (+ sekai-y (* 1/2 zoom-ratio panel-height)) 'single-float))
       )))

(defun move-camera! (camera delta-y delta-x)
  (with-slots (sekai-y sekai-x (ratio sekai-scale-unit--ratio--camera-scale-unit)) camera
    (setf sekai-y 
          (+ sekai-y (* (/ 1 ratio) (- delta-y))))
    (setf sekai-x
          (+ sekai-x (* (/ 1 ratio) (- delta-x))))))


(defun magnificate-camera! (camera expansion-or-reduction)
  (let ((next-ratio
          (* (camera-sekai-scale-unit--ratio--camera-scale-unit camera)
             (cond ((eq expansion-or-reduction :expansion) (* 1.15))
                   ((eq expansion-or-reduction :reduction) (/ 1.15))
                   (t 1.00)))))
    (setf (camera-sekai-scale-unit--ratio--camera-scale-unit camera)
          next-ratio)))


;;; sekai-coordinate <-> camera-coordinate

(defun cameraed-panel-measure-to-sekai-measure
    (cameraed-panels-y cameraed-panels-x camera space-scale)
  space-scale
  (with-slots ((ratio sekai-scale-unit--ratio--camera-scale-unit)
               (camera-y sekai-y) (camera-x sekai-x) panel-height panel-width) camera
    (vec (+ (/ (- cameraed-panels-y (* 1/2 panel-height)) ratio) camera-y)
         (+ (/ (- cameraed-panels-x (* 1/2 panel-width)) ratio) camera-x))))

(defun sekai-measure-to-cameraed-measure
    (sekai-scale-unit--ratio--camera-scale-unit sekais-measure
     camera-center-of-the-sekai panel-length)
  (+ (* sekai-scale-unit--ratio--camera-scale-unit (- sekais-measure camera-center-of-the-sekai))
     (* 1/2 panel-length)))


(defun cameraed-panel-aabb-to-sekai-aabb (panels-aabb camera space-scale)
  (let ((top-left (cameraed-panel-measure-to-sekai-measure
                   (aabb-top panels-aabb) (aabb-left panels-aabb) camera space-scale))
        (bottom-right (cameraed-panel-measure-to-sekai-measure
                       (aabb-bottom panels-aabb) (aabb-right panels-aabb) camera space-scale)))
    (make-aabb :left (vec-dx top-left) :right (vec-dx bottom-right)
               :top (vec-dy top-left) :bottom (vec-dy bottom-right))))


;;; camera colliion-detection

(defun collisiones-to-camera (camera uniformed-grid space-scale num-grid)
  (aabb--uniformed-grid--collision-detection--omitted
   (camera-aabb camera) uniformed-grid space-scale num-grid))




;;; select-lifes


(defstruct select-lifes
  (selecting-p nil)
  (panels-aabb (make-aabb))
  (selectings '()))

