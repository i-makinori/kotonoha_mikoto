(in-package :kotonoha_mikoto)


;; sousei

(defun sekai-no-hazimari-terrains-sim ()

  ;; space
  (defparameter *space-height* 1440) ;; 144000
  (defparameter *space-width* 1440)
  (defparameter *equality-sunlight* 0.30) ;; 全体に等価な太陽光
  
  ;;terrain
  (defparameter *num-terrains-col* 32)
  (defparameter *num-terrains-row* 32)
  
  ;; substance
  (defparameter *num-substance* 1) ;; 64000
  (defparameter *num-grid* 14) ;;512
  (defparameter *substance-speed-max* 0.50) 
  
  (defparameter *substance-radius* #'(lambda () (float (+ 10 (random 10)))))
  

  (let* ((scale (make-space-scale :height *space-height* :width *space-width*))
         (sekai (sousei
                 :space-scale scale
                 :uniformed-grid (liner-uniformed-grid-2d-space *num-grid*)
                 :ikitoshi-ikerumono (hazimarino-hi-ni-iketoshimono scale))))
    (mapcar ;; push to uniformed grid
     #'(lambda (substance)
         (push-substance-to-uniformed-grid-sequence!
          (sekai-no-uniformed-grid sekai)
          (substance-uniformd-grid-indexes substance (sekai-no-space-scale sekai) *num-grid*)
          substance))
     (sekai-substances sekai))
    (mapcar ;; collisionings as substance
     #'(lambda (substance)
         (setf (substance-collisioning substance)
               (substance--uniformed-grid--collision-detection 
                substance (sekai-no-uniformed-grid sekai) (sekai-no-space-scale sekai) *num-grid*)))
     (sekai-substances sekai))
    sekai))

;; updation

(defun update-terrains-sim! (interention sekai!)
  interention
  ;; terrains
  (update-terrains! (sekai-no-terrains sekai!) (sekai-no-current-time sekai!))
  ;; parametes
  (setf (sekai-no-current-time sekai!) (+ 1 (sekai-no-current-time sekai!)))
  ;;
  sekai!)
