(in-package :kotonoha_mikoto)

;;; iketoshimono


;;; iketoshimono parameters

(defstruct (iketoshimono-environment-parameters)
  (calculble-cost *calculable-cost*) ;; 死までに費やせる計算コスト
  (memorable-space *memorable-space*) ;; 利用可能なメモリ空間  
  ;;
  (act-of-this-period nil)
  (thought-of-this-period-move (vec 0 0))
)



;;; ikitoshi ikerumono

(defstruct (iketoshimono (:constructor umareizuru) (:conc-name iketoshimono-no-))
  (substance (make-substance) :type substance) ;; 物質,肉体
  (enviroment-parameters (make-iketoshimono-environment-parameters) ;; 環境パラメータ
                         :type iketoshimono-environment-parameters) 
  (program '())
  ;;(gene '()) ;; 遺伝子
  )



;;; programmable functions of iketoshimono

(defun directions-of-light (iketoshimono sekai)
  (let* ((terrains (sekai-no-terrains sekai))
         (coordinate (substance-coordinate (iketoshimono-no-substance iketoshimono))))
    (lights-of-on-radius
     (sekai-dimension-to-terrain-dimension *space-height* (vec-dy coordinate) *num-terrains-row*)
     (terrain-dimension-to-sekai-dimension *space-width* (vec-dx coordinate) *num-terrains-col*)
     *maximam-sense-range*
     terrains)))

(defun directions-of-water (iketoshimono sekai)
  (let* ((terrains (sekai-no-terrains sekai))
         (coordinate (substance-coordinate (iketoshimono-no-substance iketoshimono))))
    (waters-of-on-radius
     (sekai-dimension-to-terrain-dimension *space-height* (vec-dy coordinate) *num-terrains-row*)
     (terrain-dimension-to-sekai-dimension *space-width* (vec-dx coordinate) *num-terrains-col*)
     *maximam-sense-range*
     terrains)))

(defun want-to-move (iketoshimono sekai vec-of-direction)
  sekai
  (let ((thought (iketoshimono-environment-parameters-thought-of-this-period-move
                  (iketoshimono-no-enviroment-parameters iketoshimono))))
    (setf (iketoshimono-environment-parameters-thought-of-this-period-move
                  (iketoshimono-no-enviroment-parameters iketoshimono))
          (vec-add thought vec-of-direction))))

;;; I/O programmable functions of iketoshimono


(defun iketoshimono-move (iketoshimono sekai)
  sekai
  (let* ((coordinate! (substance-coordinate (iketoshimono-no-substance iketoshimono)))
         (thought-of-move (iketoshimono-environment-parameters-thought-of-this-period-move
                           (iketoshimono-no-enviroment-parameters iketoshimono)))
         (dist-of-thought (sqrt (+ (expt (vec-dy thought-of-move) 2)
                                   (expt (vec-dx thought-of-move) 2)))))
    (setf (substance-coordinate (iketoshimono-no-substance iketoshimono))
          (vec-add coordinate!
                   (if (< dist-of-thought *substance-speed-max*)
                       thought-of-move
                       (vec (* *substance-speed-max* (/ (vec-dy thought-of-move) dist-of-thought))
                            (* *substance-speed-max* (/ (vec-dx thought-of-move) dist-of-thought))))))
    iketoshimono))



;; move on uniformed grid

(defun substance-update-uniformed-grid-sequence!-for-destructed-update-substance!
    (uniformed-grid! previous-substance-indexes updated-substance space-scale num-grid)
  (substance-update-uniformed-grid-sequence!-for-destructed-update-substance!
   uniformed-grid!
   previous-substance-indexes
   updated-substance
   space-scale num-grid))


(defun iketoshimono-move-on-uniformed-grid!
    (iketoshimono sekai)
  (let* ((space-scale (sekai-no-space-scale sekai))
         (previous-substances (iketoshimono-no-substance iketoshimono))
         ;;
         (previous-substance-uniformed-grid-indexes
           (substance-uniformd-grid-indexes previous-substances space-scale *num-grid*))
         (moved-iketoshimono
           (iketoshimono-move iketoshimono sekai))
         )
    (substance-update-uniformed-grid-sequence!
     (sekai-no-uniformed-grid sekai)
     previous-substance-uniformed-grid-indexes
     (iketoshimono-no-substance moved-iketoshimono)
     space-scale *num-grid*))
  iketoshimono)


;;; updation

(defun update-iketoshimono-parameters (iketoshimono! sekai)
  sekai
  (with-slots (calculble-cost memorable-space thought-of-this-period-move)
      (iketoshimono-no-enviroment-parameters iketoshimono!)
    (setf calculble-cost (- calculble-cost 1))
    (setf memorable-space memorable-space)
    (setf thought-of-this-period-move (vec 0.00 0.00)))
  iketoshimono!)

(defun iketshimono-think! (iketoshimono! sekai)
  sekai
  (let* ((directions-of-light (directions-of-light iketoshimono! sekai))
         (direction (if (and directions-of-light
                             (< 0.001 (cdr (car directions-of-light))))
                        (car (car directions-of-light))
                        (vec 0.00 0.00))))
    (want-to-move iketoshimono! sekai direction)
    iketoshimono!))

(defun iketoshimono-act! (iketoshimono! sekai)
  sekai
  (progn
    ;; move on uniformed-grid
    (iketoshimono-move-on-uniformed-grid! iketoshimono! sekai)
    ;; others
  iketoshimono!
  ))

(defun update-iketoshimono! (iketoshimono! sekai)
  (setf iketoshimono! (iketshimono-think! iketoshimono! sekai))
  (setf iketoshimono! (iketoshimono-act! iketoshimono! sekai))
  (setf iketoshimono! (update-iketoshimono-parameters iketoshimono! sekai)))


