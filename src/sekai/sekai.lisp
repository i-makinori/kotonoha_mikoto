
(in-package :kotonoha_mikoto)


;;; parameters

;; space
(defparameter *space-height* 1440) ;; 144000
(defparameter *space-width* 1440)
(defparameter *equality-sunlight* 0.30) ;; 全体に等価な太陽光

;;terrain
(defparameter *num-terrains-col* 32)
(defparameter *num-terrains-row* 32)
(defparameter *terrains-light-max* 144000)
(defparameter *terrains-water-max* 144000)

;; substance
(defparameter *num-substance* 32) ;; 64000
(defparameter *num-grid* 14) ;;512
(defparameter *substance-speed-max* 0.50) 

(defparameter *substance-radius* #'(lambda () (float (+ 10 (random 10)))))

;; iketoshimono
(defparameter *calculable-cost* 12800) ;; 死までの関数呼び出し回数｡
(defparameter *memorable-space* 12800) ;; 最大メモリビット
(defparameter *maximam-sense-range* 3) ;; 最大観測半径





;;; sekai

(defstruct (sekai (:conc-name sekai-no-) (:constructor sousei))
  (current-time 0)
  (space-scale (make-space-scale))
  (terrains (make-terrains))
  (ikitoshi-ikerumono nil)
  ;;
  (uniformed-grid (liner-uniformed-grid-2d-space *num-grid*)))

(defun sekai-substances (sekai)
  (mapcar #'iketoshimono-no-substance (sekai-no-ikitoshi-ikerumono sekai)))


;;; sousei

(defun randomize-substance (space-scale)
  (make-substance
   :unique-key (gensym)
   :coordinate 
   (vector (random (+ 0.00 (space-scale-height space-scale)))
           (random (+ 0.00 (space-scale-width space-scale))))
   :radius (funcall *substance-radius*)
   :speed (vector (- *substance-speed-max* (random (* 2 *substance-speed-max*)))
                  (- *substance-speed-max* (random (* 2 *substance-speed-max*))))
   :mass (+ 1 (random 100))))

(defun hazimarino-hi-ni-iketoshimono (space-scale)
  (mapcar
   #'(lambda (n) n
       (umareizuru
        :substance (randomize-substance  space-scale)))
   (upto 1 *num-substance*)
))


(defun sekai-no-hazimari-no-hi ()
  ;; space
  (defparameter *space-height* 1440) ;; 144000
  (defparameter *space-width* 1440)
  
  ;;terrain
  (defparameter *num-terrains-col* 32)
  (defparameter *num-terrains-row* 32)
  
  ;; substance
  (defparameter *num-substance* 32) ;; 64000
  (defparameter *num-grid* 14) ;;512
  (defparameter *substance-speed-max* 0.50)
  (defparameter *substance-radius* #'(lambda () (float (+ 10 (random 10)))))
  
  (defparameter *maximam-sense-range* 3) ;; 最大観測半径

  ;;;;

  (let* ((scale (make-space-scale :height *space-height* :width *space-width*))
         (sekai (sousei
                 :space-scale scale
                 :uniformed-grid (liner-uniformed-grid-2d-space *num-grid*)
                 :ikitoshi-ikerumono (hazimarino-hi-ni-iketoshimono scale))))
    (mapcar ;; push to uniformed grid
     #'(lambda (substance)
         (push-substance-to-uniformed-grid-sequence!
          (sekai-no-uniformed-grid sekai)
          (substance-uniformd-grid-indexes substance (sekai-no-space-scale sekai) *num-grid*)
          substance))
     (sekai-substances sekai))
    (mapcar ;; collisionings as substance
     #'(lambda (substance)
         (setf (substance-collisioning substance)
               (substance--uniformed-grid--collision-detection 
                substance (sekai-no-uniformed-grid sekai) (sekai-no-space-scale sekai) *num-grid*)))
     (sekai-substances sekai))
    sekai))

(defparameter *sample-sekai* (sekai-no-hazimari-no-hi))



;;; atarashiki sekai

(defun update-ikerumono! (processed-interention sekai)
  processed-interention
  (let ((ikerumono (sekai-no-ikitoshi-ikerumono sekai)))
    (mapcar 
     #'(lambda (iketoshimono)
         (update-iketoshimono! iketoshimono sekai))
     ikerumono)
    (mapcar ;; collisionings as substance
     #'(lambda (substance)
         (setf (substance-collisioning substance)
               (substance--uniformed-grid--collision-detection 
                substance (sekai-no-uniformed-grid sekai) (sekai-no-space-scale sekai) *num-grid*)))
     (sekai-substances sekai))
    sekai))

;;; update-sekai!

(defun update-sekai! (interention sekai!)
  interention
  ;; terrains
  (update-terrains! (sekai-no-terrains sekai!) (sekai-no-current-time sekai!))
  ;; iketumono
  (update-ikerumono! interention sekai!)
  ;; parametes
  (setf (sekai-no-current-time sekai!) (+ 1 (sekai-no-current-time sekai!)))
  sekai!)

;;; sousei

(defun sekai-no-hazimari (current-simulation)
  (case current-simulation
    ((:phototaxis :recents) (sekai-no-hazimari-no-hi))
    ((:substances) (sekai-no-hazimari-substance-sim))
    ((:terrains) (sekai-no-hazimari-terrains-sim))
    ))


;;; call update sekai

(defun update-sekai (interention sekai! current-simulation)
  (let ((function-update-sekai
          (case current-simulation
            ((:phototaxis :recents) #'update-sekai!)
            ((:substances) #'update-substances!)
            ((:terrains) #'update-terrains-sim!)))
        )
    (funcall function-update-sekai interention sekai!)
    sekai!))

