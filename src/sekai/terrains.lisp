
(in-package :kotonoha_mikoto)

;;; utils

(defun distance (p1 p2)
  (abs (- p1 p2)))


(defun list-of-y-x-element-on-radius (center-y center-x radius matrix)
  (let ((collected
          (loop for y from (round (- center-y radius)) to (round (+ center-y radius))
                collect (loop for x from (round (- center-x radius)) to (round (+ center-x radius))
                              collect (if (and (>= radius (distance y center-y))
                                               (>= radius (distance x center-x)))
                                          (handler-case
                                              (list y x (aref matrix y x))
                                            (sb-int:invalid-array-index-error nil))
                                          nil)))))
    (remove nil (apply #'append collected))))


(defun lights-of-on-radius (center-y center-x radius terrains)
  (let* ((coord-and-elements (list-of-y-x-element-on-radius center-y center-x radius terrains))
         (coord-and-elements-sorted
           (sort coord-and-elements
                 #'(lambda (c1 c2) (> (terrain-cell-light (caddr c1))
                                      (terrain-cell-light (caddr c2)))))))
    (mapcar #'(lambda (y-x-cell)
                (let ((y (car y-x-cell))
                      (x (cadr y-x-cell))
                      (cell (caddr y-x-cell)))
                  (cons (vec (- y center-y)
                             (- x center-x))
                        (terrain-cell-light cell))))
            coord-and-elements-sorted
    )))

(defun waters-of-on-radius (center-y center-x radius terrains)
  (let* ((coord-and-elements (list-of-y-x-element-on-radius center-y center-x radius terrains))
         (coord-and-elements-sorted
           (sort coord-and-elements
                 #'(lambda (c1 c2) (> (terrain-cell-water (caddr c1))
                                      (terrain-cell-water (caddr c2)))))))
    (mapcar #'(lambda (y-x-cell)
                (let ((y (car y-x-cell))
                      (x (cadr y-x-cell))
                      (cell (caddr y-x-cell)))
                  (cons (vec (- center-y y)
                             (- center-x x))
                        (terrain-cell-water cell))))
            coord-and-elements-sorted
    )))


;;; terrains

(defstruct (terrain-cell)
  (light 0.00)
  (water 0.00))

(defun make-terrains ()
  (let ((terrains (make-array (list *num-terrains-row* *num-terrains-col*)
                              :initial-element (make-terrain-cell))))
    (iter-matrix terrains
                 #'(lambda (y x matrix)
                     (setf (aref matrix y x) (make-terrain-cell))))
    terrains))


(defun sekai-dimension-to-terrain-dimension (sekai-scale sekai-point num-terrains-of-dimension)
  (* sekai-point (/ num-terrains-of-dimension sekai-scale)))

(defun terrain-dimension-to-sekai-dimension (num-terrains-of-dimension terrain-point sekai-scale)
  (* terrain-point (/ sekai-scale num-terrains-of-dimension)))

(defun format-sekai-terrains (sekai)
  (with-slots (terrains) sekai
    (format t "    ")
    (loop for x from 0 to (1- (array-dimension terrains 1))
          do (format t "~2A     " x))
    (loop for y from 0 to (1- (array-dimension terrains 0))
          do ;; terrain light
             (format t "~%~2AL:" y)
             (loop for x from 0 to (1- (array-dimension terrains 1))
                   do (format t "~,3f| " (terrain-cell-light (aref terrains y x))))
             ;; terrain water
             (format t "~%  W:")
             (loop for x from 0 to (1- (array-dimension terrains 1))
                   do (format t "~,3f| " (terrain-cell-water (aref terrains y x)))))))

;; updation

(defun light-circulation (time terrains)
  (let* ((effect-radius 2)
         (one-cycle-time 440)
         ;;
         (height (array-dimension terrains 0))
         (width (array-dimension terrains 0))
         (phase-y (+ (/ height 2)
                     (* (sin (* pi 2 (/ time one-cycle-time)))
                        height 0.35)))
         (phase-x (+ (/ width 2)
                     (* (cos (* pi 2 (/ time one-cycle-time)))
                        width 0.35))))
    (iter-matrix terrains
                #'(lambda (y x matrix)
                    (let ((on-light
                            (if (and (>= effect-radius (distance y phase-y))
                                     (>= effect-radius (distance x phase-x)))
                                1.00 0.00)))
                      (setf (terrain-cell-light (aref terrains y x))
                            (+ (* (terrain-cell-light (aref matrix y x)) 0.95)
                               on-light)))))
    terrains))

(defun water-circulation (time terrains)
  (let* ((effect1-radius 2)
         (effect2-radius 4)
         (one-cycle-time 660)
         ;;
         (height (array-dimension terrains 0))
         (width (array-dimension terrains 0))
         (phase1-y (+ (* height 1/4)
                      (* (sin (* pi 2 (/ time one-cycle-time)))
                         height 0.175)))
         (phase1-x (+ (* width 1/4)
                      (* (cos (* pi 2 (/ time one-cycle-time)))
                         width 0.175)))
         (phase2-y (+ (* height 2/4)
                      (* (sin (* pi 2 (/ time one-cycle-time)))
                         height 0.35)))
         (phase2-x (+ (* width 3/4)
                      (* (cos (* pi 2 (/ time one-cycle-time)))
                         width 0.175))))
    (iter-matrix terrains
                #'(lambda (y x matrix)
                    (let ((on-water
                            (if (or (and (>= effect1-radius (distance y phase1-y))
                                         (>= effect1-radius (distance x phase1-x)))
                                    (and (>= effect2-radius (distance y phase2-y))
                                         (>= effect2-radius (distance x phase2-x))))
                                1.00 0.00)))
                      (setf (terrain-cell-water (aref terrains y x))
                            (+ (* (terrain-cell-water (aref matrix y x)) 0.95)
                               on-water)))))
    terrains))

(defun update-terrains! (terrains time)
  (light-circulation time terrains)
  (water-circulation time terrains)
  )

