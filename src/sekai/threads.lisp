(in-package :kotonoha_mikoto)

;;;


(defparameter *max-timeout-per-iketoshimono 0.005)

(defun thread-test1 ()
  (print "test start")
  (print "create thread")
  (let ((th (portable-threads:spawn-thread "foo"
                                           (lambda ()
                                             (let ((*standard-output* *standard-output*))
                                               (print "start thread")
                                               (sleep 0.001)
                                               (print "end thread")))))
        (num 0))
    (loop :while (thread-alive-p th)
          :do (incf num))
    (print num)
    (print "test end")))

(defun thread-test2 ()
  (portable-threads:with-timeout
      (3 (print 20))
    (loop for y from 0 to 10
          do (print y)
             (sleep 1))))



(defun make-thread-of-iketoshimono (iketoshimono sekai)
  ;; (make-thread 
  )

(defun calculate-iketoshimono (iketoshimono sekai)
  (let ((program (iketoshimono-no-data-and-program iketoshimono)))
    (portable-threads:with-timeout
        (*max-timeout-per-iketoshimono*
         (save-current-continuation)
         )
      )))
    

;;; something tests

;;; computer functions

;; http://www.paulgraham.com/rootsoflisp.html
;; http://ep.yimg.com/ty/cdn/paulgraham/jmc.lisp

(defun _null (object)
  (eq object '()))

(defun _not (form)
  (cond (form nil)
        (t t)))

(defun _and (form1 form2)
  (cond (form1 form2)
        (t nil)))

(defun _or (form1 form2)
  (cond (form1 form1)
        (form2 form2)
        (t nil)))

(defun _append (list1 list2)
  (cond ((null list1) list2)
        (t (cons (car list1) (_append (cdr list1) list2)))))

(defun _list (&rest object)
  (cond ((_null object) nil)
        (t (cons (car object) (apply #'_list (cdr object))))))

(defun _assoc (x y)
  (cond ((eq (caar y) x) (cadar y))
        ((_null y) nil)
        ('t (_assoc x (cdr y)))))

(defun _pair (x y)
  (cond ((_and (_null x) (_null y)) '())
        ((_and (_not (atom x)) (_not (atom y)))
         (cons (_list (car x) (car y))
               (_pair (cdr x) (cdr y))))))


(defun _eval_cond (c m)
  (cond ((_eval (caar c) m)
         (_eval (cadar c) m))
        ('t (_evcon (cdr c) m))))


(defun _eval_list (m a)
  (cond ((_null m) '())
        ('t (cons (_eval (car m) a)
                  (_eval_list (cdr m) a)))))
#|

(defun _eval_list (p m)
  (cond ((_null (cdr p)) (_eval (car p) m))
        ('t (_evlis (cdr p)
                    (_eval (car p) m)))))
      
|#

(defun _eval_func (func cdr_p m)
  (cond
    ;; primitive
    ((eq func 'quote) (car cdr_p))
    ((eq func 'atom) (atom (_eval (car cdr_p) m)))
    ((eq func 'eq) (eq (_eval (car cdr_p) m)
                       (_eval (cadr cdr_p) m)))
    ((eq func 'car) (car (_eval (car cdr_p) m)))
    ((eq func 'cdr) (cdr (_eval (car cdr_p) m)))
    ((eq func 'cons) (cons (_eval (car cdr_p) m)
                           (_eval (cadr cdr_p) m)))
    ;;
    ((eq func 'cond) (_eval_cond cdr_p m))
    ;;((eq func 'let*) (_eval_let* (car cdr_p) (cadr cdr_p) m))
    ;; funcall on memo
    ('t 
     (cond ((null (_assoc func m))
            (format t "unnown function: ~A~%" func)
            nil)
           (t
            (_eval (cons (_assoc func m) cdr_p) m))))))

#|
(defun _eval_let* (p_bindings p_body m)
  (cond ((_null p_bindings) (_eval p_body m))
        (t (_eval_let*
            (cdr p_bindings)
            p_body
            (cons (_list (caar p_bindings)
                         (_eval (cadar p_bindings) m))
                  m)))))
|#

(defun _eval (p m)
  ;; p:program m:memos
  (format t "prog: ~A~%memo:~A~%---~%" p m)
  (cond
    ;; atom
    ((_atom p) (_assoc p m))
    ;; function
    ((atom (car p))
     (_eval_func (car p) (cdr p) m))
    ;; special form
    ((eq (caar p) 'label)
     (_eval (cons (caddar p) (cdr p))
            (cons (list (cadar p) (car p))
                  m)))
    ((eq (caar p) 'lambda)
     (_eval (caddar p)
            (_append (_pair (cadar p) (_eval_list (cdr p) m))
                     m)))
    ))


;; computer


(defstruct computer
  (data-and-program nil)
  (consed-pointer (list #'identity))
  (freezedes nil)
  )



           

(defun eval-program-of-computer (program memos cons-pointer)
  ())

