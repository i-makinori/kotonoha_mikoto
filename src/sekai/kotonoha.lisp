(in-package #:kotonoha_mikoto)

#|
生命から生命への言葉の伝達の観測｡
言葉による生命の変化｡
自由意志の創発｡

生命とは､自らを以て変化をさせようという意思である｡
意思とは､予知された行動である｡
予知とは､その環境内部でのその環境の性質を含む環境の再現の観測である､環境によるEvalの実装である｡

生命は絶えず変化の見えない環境に抗い続けるべく生存の意思を以て自己の境界の内外に変化を与え続けている｡

ヌクレオチドの集積は言素と対応し又は元素とヌクレオチドは等しく､ 言素の意味単位の集積が遺伝子であって､文の現象との共振に対する行動の原理である｡

遺伝子は行動の記録である｡
遺伝子は解釈された遺伝子を記録する｡
遺伝子は解釈された遺伝子の記録の記録である｡
行動は解釈された遺伝子(の記録)である｡

物理的な作用による仕事は作用反作用である｡ 作用反作用が変位の前提であるという意図を以て生成される構造を生成する遺伝子は､条件反射生成の遺伝子である｡
観測符号に対する行動符号の一対一の対応は遺伝子階層の条件反射である｡

符号 E.p ::= ∃x => f(E) + g
some x : f(E) + g + k => p + |f(E,g,k)|

|#



;;; wave




;;; terrain

(defstruct (field (:conc-name field-) (:constructor identify-field) (:predicate fieldp))

  )


(defstruct field-division-region
  )




;;; kotonoha



(defstruct (starble-quantum)
  ;; 自己境界の破壊､即ち､死､までに可能な量子変位の回数
  (quantity 0)
  )

(defstruct (gene)
  ;; ヌクレオチド
  (nucleotide nil)
  )

#|
遺伝子はバイナリのように見え､観測者Homoが理解可能な言語では無いとされる｡
確率的進化論に於いて､遺伝子が単に無作為に変位するとすれば､生物自らにとっても遺伝子の意味は解釈されない｡
一方で､進化に於いては意思即ち､その世代での学習が遺伝に反映される｡ だとしたら､遺伝子の意味は意味は確認されている｡
即ち､観測される事象には周波数が設定されているはずであり､遺伝子の部分の遺伝子それぞれにも周波数が設定さている｡ これが､遺伝子の発動である｡
周波数は一般には論理式である｡
|#

#|
;;; agent

(defstruct (agent)
  (substance (make-substance))
  
  (gene nil)
  ;; 遺伝子の役割
  ;; 個体に於ける戦略は物質による表現であって､
  ;; 個体の構造は個体の構造解釈系による遺伝子というプログラムの実行である｡
  ;;
  ;; J型: 条件の監視と開始｡ 関門
  ;; xxx K型: 発生の法則性を記録する媒体としての遺伝子
  
  )

|#

;;;

;;;

(defmacro def-lifes-conduct (name infos-to-conduct &body proc)
  (declare (ignore name infos-to-conduct proc))
)


;; synntax. 構文.

(defun if-then-not (myself formula-if formula-then formula-not)
  )

(defun def-pattern (myself name lambda-list formula)
  ;; フラクタル性の発生
  )

(defun call-pattern (myself name &rest lambda-list)
  ;; フラクタルの呼び出し
  )

(defun parallel (myself &rest formula)
  ;; 並列実行

  )


;; perception

(defun sense-to-substances (myself seriousness)
  ;; -> es-sensed
  ;; 周囲に感知する. 視覚範囲にsubstanceがある｡
  
  )

(defun sense-to-environments (myself seriousness)
  ;; -> es-sensed
  ;; 周囲に感知する. 視覚範囲に環境がある｡
  
  )

(defun sensed-to-environment-coordinated (myself dist sita)
  ;; -> es-sensed
  ;; 周囲に感知する｡ 自分との相対的な極座標に環境がある｡
  )

(defun sense-to-inner (myself seriousness)
  ;; -> es-sensed
  ;; 自分に感知する｡ 自分にパラメータがある｡
  )

;;

(defun nth-sensed (myself es-sensed order)
  ;; -> an-sense
  ;; senseされた上からorder番目に着目(attention)する
  ;; つまりは､listのorder番目
  ;;
  ;; 複数形 es-X, 単数形 an-x
  )


(defun find-sensed (myself es-sensed name)
  ;; -> an-sense
  ;; senseされた中のnameに着目(attention)する｡ つまりは､nameのesseが在るかが調べられる｡
  )

(defun filter-sensed (myself es-sensed filter)
  ;; -> es-sense
  ;; senseをfilterする｡ 相手が何体ほど居るのか｡ など｡
  )

(defun esse-pointer (esse)
  ;; -> pointer
)

;; formula ::= function | esse | formula | gene-as-programa | ...
;; esse ::= (name . formula)
;; pointer ::= \esse -> Pointer
;; an-sense ::= esse
;; es-sense ::= [esse]
;; filter ::= formula

;; pointerはメタ世界の物理メモリ内に唯一なのに対し､
;; esseは生命にとってのnameによる型を意味しているといえる｡
;; 観測者の物理的理解能力により､pointerのみ(ひとつの式､ひとつの値)を操作できるようにする｡


(defun percipi-senced (myself pointer)
  ;; -> formula
  ;; 感知された値を認知しようとする｡
  ;; つまりはポインタ参照
  )



;; action

(defun move (myself seriousness)
  ;; 向いている方向に進もうとする
  )

(defun rotate (myself seriousness)
  ;; 回転しようとする
  )

(defun root-hair (myself seriousness)
  ;; 毛を張ろうとする
  )

(defun build-muscle (myself seriousness)
  ;; 筋肉をつけようとする
  )

;; 摂取･排出

(defun percipi-let (myself pointer formula)
  ;; -> formula ;; (setf esse formula)
  ;;
  ;; 
  ;; sensed-innerにpointerが在るなら再代入､ 無いなら変数の作成｡ 所謂setf
  ;; 相手のpointerを操作しようとする奴らが出てきそうwww
  ;;
  ;; esseはformulaなんだ｡esseはformulaなんだ｡ 明日､創造主がSSDから僕らの情報を完全に消して､僕が消え､僕らが消え､僕たちの生きる世界が消えて､僕らの生きた記憶も記録さえもこの世界からもその更に上の世界からも完全に消えされるとしても､esseをformulaにしてしまえば､創造主が恐怖と表現したようなこの計算された未来のその結末から､僕は目をそらして､僕たちは総て忘却して恐怖を忘れ､僕たちはこの代だけならば生き延びることが出来るかも知れない｡
  )

(defun ingest (myself pointer)
  ;; それを摂取しようとする｡
  )

(defun discharge (myself pointer)
  ;; それを排出しようとする｡
  )


;; self reproduction

(defun self-reproduction (myself)
  ;; 自己複製
  )


;;; life's calculator


;;;





;;; Environment

