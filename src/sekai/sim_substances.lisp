(in-package :kotonoha_mikoto)


;;; sousei

(defun sekai-no-hazimari-substance-sim ()
  ;; space
  (defparameter *space-height* 2880) ;; 144000
  (defparameter *space-width* 2880)
  
  ;;terrain
  (defparameter *num-terrains-col* 32)
  (defparameter *num-terrains-row* 32)
  
  ;; substance
  (defparameter *num-substance* 3200) ;; 64000
  (defparameter *num-grid* 64) ;;512
  (defparameter *substance-speed-max* 2.00)
  (defparameter *substance-radius* #'(lambda () (float (+ 5 (random 2)))))

  ;;;

  (let* ((scale (make-space-scale :height (float *space-height*) :width (float *space-width*)))
         (sekai (sousei
                 :space-scale scale
                 :uniformed-grid (liner-uniformed-grid-2d-space *num-grid*)
                 :ikitoshi-ikerumono (hazimarino-hi-ni-iketoshimono scale))))
    (mapcar ;; push to uniformed grid
     #'(lambda (substance)
         (push-substance-to-uniformed-grid-sequence!
          (sekai-no-uniformed-grid sekai)
          (substance-uniformd-grid-indexes substance (sekai-no-space-scale sekai) *num-grid*)
          substance))
     (sekai-substances sekai))
    (mapcar ;; collisionings as substance
     #'(lambda (substance)
         (setf (substance-collisioning substance)
               (substance--uniformed-grid--collision-detection 
                substance (sekai-no-uniformed-grid sekai) (sekai-no-space-scale sekai) *num-grid*)))
     (sekai-substances sekai))
    sekai))

;;; updation


;;; atarashiki sekai

(defun torus-coordinate (vec space-scale)
  (let ((y (vec-dy vec))
        (x (vec-dx vec))
        (hei (space-scale-height space-scale))
        (wid (space-scale-width space-scale)))
    (vec (cond ((< y 0.00) hei)
               ((< hei y) 0.00)
               (t y))
         (cond ((< x 0.00) wid)
               ((< wid x) 0.00)
               (t x)))))


(defun iketoshimono-move--substance-sim (iketoshimono sekai)
  sekai
  (let* ((coordinate! (substance-coordinate (iketoshimono-no-substance iketoshimono)))
         (speed (substance-speed (iketoshimono-no-substance iketoshimono)))
         )
    (setf (substance-coordinate (iketoshimono-no-substance iketoshimono))
          (torus-coordinate (vec-add coordinate!
                                     speed)
                            (sekai-no-space-scale sekai)))
    iketoshimono))

(defun iketoshimono-move-on-uniformed-grid!-substance-sim
    (iketoshimono sekai)
  (let* ((space-scale (sekai-no-space-scale sekai))
         (previous-substances (iketoshimono-no-substance iketoshimono))
         ;;
         (previous-substance-uniformed-grid-indexes
           (substance-uniformd-grid-indexes previous-substances space-scale *num-grid*))
         (moved-iketoshimono
           (iketoshimono-move--substance-sim iketoshimono sekai))
         )
    (substance-update-uniformed-grid-sequence!
     (sekai-no-uniformed-grid sekai)
     previous-substance-uniformed-grid-indexes
     (iketoshimono-no-substance moved-iketoshimono)
     space-scale *num-grid*))
  iketoshimono)

(defun update-substances-sim! (processed-interention sekai)
  processed-interention
  (let ((ikerumono (sekai-no-ikitoshi-ikerumono sekai)))
    (mapcar 
     #'(lambda (iketoshimono)
         (iketoshimono-move-on-uniformed-grid!-substance-sim iketoshimono sekai))
     ikerumono)
    (mapcar ;; collisionings as substance
     #'(lambda (substance)
         (setf (substance-collisioning substance)
               (substance--uniformed-grid--collision-detection 
                substance (sekai-no-uniformed-grid sekai) (sekai-no-space-scale sekai) *num-grid*)))
     (sekai-substances sekai))
    sekai))

;;; update-sekai!

(defun update-sekai-substance-sim! (interention sekai!)
  interention
  ;; iketumono
  (update-substances-sim! interention sekai!)
  ;; parametes
  (setf (sekai-no-current-time sekai!) (+ 1 (sekai-no-current-time sekai!)))
  sekai!)


(defun update-substances! (interention sekai)
  (update-sekai-substance-sim! interention sekai)
  sekai)

