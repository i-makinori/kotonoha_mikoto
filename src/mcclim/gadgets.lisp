
(in-package :kotonoha_mikoto)


;;; mcclim-gadgets



;;; sekai-status

;; minimap

(defclass sekai-status-minimap-pane (clime:never-repaint-background-mixin basic-gadget)
  ())

(defun draw-minimap (pane camera sekai)
  (let* ((space-scale (sekai-no-space-scale sekai))
         (sekai-height (space-scale-height space-scale))
         (sekai-width (space-scale-width space-scale))
         (sekai-aabb (make-aabb :left (float 0) :right (float sekai-width)
                                :top (float 0) :bottom (float sekai-height)))
         (camera-aabb (camera-aabb camera))
         ;;
         (panel-height-delta (- (bounding-rectangle-height pane) 0))
         (panel-width-delta (- (bounding-rectangle-width pane) 0))
         ;;
         (top-of-camera-and-sekai (min 0 (aabb-top camera-aabb)))
         (bottom-of-camera-and-sekai (max sekai-height (aabb-bottom camera-aabb)))
         (right-of-camera-and-sekai (max sekai-width (aabb-right camera-aabb)))
         (left-of-camera-and-sekai (min 0 (aabb-left camera-aabb)))
         ;;
         (width-of-camera-and-sekai
           (- right-of-camera-and-sekai left-of-camera-and-sekai))
         (height-of-camera-and-sekai
           (- bottom-of-camera-and-sekai top-of-camera-and-sekai))
         ;;
         (panel-as-camera
           (make-camera
            :sekai-y (/ sekai-height 2) :sekai-x (/ sekai-width 2)
            :panel-height (float panel-height-delta) :panel-width (float panel-width-delta)
            :sekai-scale-unit--ratio--camera-scale-unit
            (min (/ panel-height-delta height-of-camera-and-sekai)
                 (/ panel-width-delta width-of-camera-and-sekai)))))
      ;; draw regions
      (with-bounding-rectangle* (x0 y0 x1 y1) (sheet-region pane)
        (climi::with-double-buffering ((pane x0 y0 x1 y1) (wtf-wtf-wtf))
          (declare (ignore wtf-wtf-wtf))
          (draw-rectangle* pane x0 y0 x1 y1 :filled t :ink +white+)
          ;; grid
          (draw-cameraed-grid pane panel-as-camera 4 space-scale)
          (draw-cameraed-aabb* ;; sekai aabb
           pane panel-as-camera space-scale sekai-aabb :ink +blue3+ :filled nil)
          (draw-cameraed-aabb* ;; camera
           pane panel-as-camera space-scale (camera-aabb camera) :ink +blue4+ :filled nil)))))

(defmethod handle-repaint ((pane sekai-status-minimap-pane) region)
  region
  (let ((camera (frame-camera (pane-frame pane)))
        (sekai (frame-current-sekai (pane-frame pane))))
    (draw-minimap pane camera sekai)))

;; interface

(defun update-sekai-status-frame-interface (frame)
  (let* ((sekai-status-minimap-pane (find-pane-named frame 'sekai-status-minimap)))
    (repaint-sheet sekai-status-minimap-pane (sheet-region sekai-status-minimap-pane))))



;;; substances status

;; list of selecting substances
(defun update-substances-status-list (frame)
  (let ((selecting-lifes (frame-select-lifes frame))
        (pane (find-pane-named frame 'substances-status-list)))
    (setf (climb::list-pane-items pane)
          (cons nil
                (mapcar #'(lambda (s) (substance-unique-key s))
                        (select-lifes-selectings selecting-lifes))))))


;; selecting-the-life

(defun draw-substances-status-text (control-string &rest format-arguments)
  (let* ((substances-status-text-stream (get-frame-pane *application-frame* 'substances-status-text))
         (pane substances-status-text-stream))
    (with-bounding-rectangle* (x0 y0 x1 y1) (sheet-region pane)
        (climi::with-double-buffering ((pane x0 y0 x1 y1) (wtf-wtf-wtf))
          (declare (ignore wtf-wtf-wtf))
          (draw-rectangle* pane x0 y0 x1 y1 :filled t :ink +white+)

          (draw-text* substances-status-text-stream
                      (apply #'format nil control-string format-arguments)
                      0 20)))))


(defun substances-status-list-update-selection (pane item)
  pane item
  (setf (frame-selecting-the-life (pane-frame pane))
        (find item (sekai-substances (frame-current-sekai (pane-frame pane)))
              ;;:test #'substance-key-equal)
              :test #'(lambda (key sub)
                        (eq key (substance-unique-key sub)))))
  (update-application-parameters-panes (pane-frame pane)))

(defun major-info-text-of-substance (substance)
  (with-slots (unique-key coordinate radius speed collisioning) substance
    (format nil "unique-key:~A~% coordinate:~A~% radius:~A~% speed:~A~% collisioning:~A~%"
            unique-key coordinate radius speed
            (mapcar #'substance-unique-key collisioning))))


;; following map of selecting-the-life

(defun project-substances-status-following-map (frame substance-to-follow sekai)
  (with-slots ((substance-coordinate coordinate) radius) substance-to-follow
    (let* ((pane (find-pane-named frame 'substances-status-following-map))
           (panel-height (coerce (bounding-rectangle-height pane) 'single-float))
           (panel-width (coerce (bounding-rectangle-width pane) 'single-float))
           ;;
           (camera
             (make-camera
              :sekai-y (vec-dy substance-coordinate) :sekai-x (vec-dx substance-coordinate)
              :panel-height panel-height :panel-width panel-width
              :sekai-scale-unit--ratio--camera-scale-unit
              (/ (min panel-height panel-width)
                 (* radius 20)))))
    (with-bounding-rectangle* (x0 y0 x1 y1) (sheet-region pane)
      (climi::with-double-buffering ((pane x0 y0 x1 y1) (wtf-wtf-wtf))
        (declare (ignore wtf-wtf-wtf))
        (projection-of-camera pane camera sekai *num-grid*))))))


;; recenter to selecting-the-life

(defun substances-status-camera-recenter (gadget)
  (let ((selecting-the-life (frame-selecting-the-life (pane-frame gadget)))
        (camera! (frame-camera (pane-frame gadget))))
    (when (substance-p selecting-the-life)
      (setf (camera-sekai-y camera!) (vec-dy (substance-coordinate selecting-the-life)))
      (setf (camera-sekai-x camera!) (vec-dx (substance-coordinate selecting-the-life))))
    nil)) 


;; update-interface

(defun update-substances-frame-interface (frame)
  (let* ((sekai (frame-current-sekai frame))
         (substances (sekai-substances sekai))
         (selecting-the-life (frame-selecting-the-life frame))
         )
    sekai substances
    ;; lists
    (update-substances-status-list frame)
    ;; texts
    (if (substance-p selecting-the-life)
        (draw-substances-status-text
         "~A~% aabb: ~A~%"
         (major-info-text-of-substance selecting-the-life)
         (substance-aabb selecting-the-life))
        (draw-substances-status-text
         "nli~%"))
    ;; following-map
    (if (and (substance-p selecting-the-life)
             (eql :follow
                  (gadget-id (gadget-value
                              (find-pane-named frame 'substances-status-show-following-map?)))))
        (project-substances-status-following-map frame selecting-the-life sekai))))

