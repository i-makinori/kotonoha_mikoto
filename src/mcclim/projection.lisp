
(in-package :kotonoha_mikoto)



;;; projection of the non converted objects

(defun draw-aabb (pane aabb &rest args)
  (apply #'draw-rectangle* pane
         (aabb-left aabb) (aabb-top aabb) (aabb-right aabb) (aabb-bottom aabb)
         args))

(defun draw-substance (pane substance &rest args)
  (apply #'draw-circle* pane
         (vec-dx (substance-coordinate substance))
         (vec-dy (substance-coordinate substance))
         (substance-radius substance)
         args))


;;; projection of the cameraed objects

(defun sekai-measure-to-cameraed-measure
    (sekai-scale-unit--ratio--camera-scale-unit sekais-measure
     camera-center-of-the-sekai panel-length)
  (+ (* sekai-scale-unit--ratio--camera-scale-unit (- sekais-measure camera-center-of-the-sekai))
     (* 1/2 panel-length)))
        
  
(defun draw-cameraed-substance (pane camera space-scale substance &rest args)
  (with-slots ((camera-y sekai-y)
               (camera-x sekai-x)
               panel-height panel-width (ratio sekai-scale-unit--ratio--camera-scale-unit))
      camera
    space-scale
    (let* ((substance-y (vec-dy (substance-coordinate substance)))
           (substance-x (vec-dx (substance-coordinate substance)))
           (substance-r (substance-radius substance)))
      (apply #'draw-circle*
             pane
             (sekai-measure-to-cameraed-measure ratio substance-x camera-x panel-width)
             (sekai-measure-to-cameraed-measure ratio substance-y camera-y panel-height)
             (* ratio substance-r)
             args))))

(defun draw-cameraed-line* (pane camera x1 y1 x2 y2 &rest args)
  (declare (optimize (speed 3) (safety 0) (debug 0))
           ;;(type camera camera) (type space-scale space-scale)
           ;;(type substance substance))
           )
  (with-slots ((camera-y sekai-y)
               (camera-x sekai-x)
               panel-height panel-width (ratio sekai-scale-unit--ratio--camera-scale-unit))
      camera
    (apply #'draw-line*
           pane
           (sekai-measure-to-cameraed-measure ratio x1 camera-x panel-width)
           (sekai-measure-to-cameraed-measure ratio y1 camera-y panel-height)
           (sekai-measure-to-cameraed-measure ratio x2 camera-x panel-width)
           (sekai-measure-to-cameraed-measure ratio y2 camera-y panel-height)
           args)))

(defun draw-cameraed-aabb* (pane camera space-scale aabb &rest args)
  (with-slots ((camera-y sekai-y)
               (camera-x sekai-x)
               panel-height panel-width (ratio sekai-scale-unit--ratio--camera-scale-unit))
      camera
    space-scale
    (with-slots (top bottom left right) aabb
      (apply #'draw-rectangle*
             pane
             (sekai-measure-to-cameraed-measure ratio left camera-x panel-width)
             (sekai-measure-to-cameraed-measure ratio top camera-y panel-height)
             (sekai-measure-to-cameraed-measure ratio right camera-x panel-width)
             (sekai-measure-to-cameraed-measure ratio bottom camera-y panel-height)
             args))))

(defun draw-cameraed-text* (pane camera text x y &rest args)
  (declare (optimize (speed 3) (safety 0) (debug 0))
           ;;(type camera camera) (type space-scale space-scale)
           ;;(type substance substance))
           )
  (with-slots ((camera-y sekai-y)
               (camera-x sekai-x)
               panel-height panel-width (ratio sekai-scale-unit--ratio--camera-scale-unit))
      camera
    (apply #'draw-text*
           pane text
           (sekai-measure-to-cameraed-measure ratio x camera-x panel-width)
           (sekai-measure-to-cameraed-measure ratio y camera-y panel-height)
           args)))


;;; def draw grid

(defmacro def-draw-grid (name lambda-list drawing-line)
  `(defun ,name ,lambda-list
     (let ((height (space-scale-height space-scale))
           (width (space-scale-width space-scale)))
       (loop for i from 0 to num-grid
             do (let* ((x (* i width (/ num-grid)))
                       (x1 x) (x2 x) (y1 0) (y2 height))
                  ,drawing-line)
                (let* ((y (* i height (/ num-grid)))
                       (y1 y) (y2 y) (x1 0) (x2 width))
                  ,drawing-line)))))


(def-draw-grid draw-cameraed-grid (pane camera num-grid space-scale)
  (draw-cameraed-line* pane camera x1 y1 x2 y2
                       :ink +green4+))


;;; draw terrains

(defun terrain-cells-colers (sekai)
  (map-matrix
   #'(lambda (cell)
       (make-rgb-color
        0.75
        (min 1.00 (max 0.00 (+ 0.75 (/ (terrain-cell-light cell) 20))))
        (min 1.00 (max 0.00 (+ 0.75 (/ (terrain-cell-water cell) 20))))))
   (sekai-no-terrains sekai)))

(defun draw-terrains (pane camera sekai)
  (let* ((space-scale (sekai-no-space-scale sekai))
         (terrains (sekai-no-terrains sekai))
         (cell-height (/ (space-scale-height (sekai-no-space-scale sekai))
                         (array-dimension terrains 0)))
         (cell-width (/ (space-scale-width (sekai-no-space-scale sekai))
                        (array-dimension terrains 1)))
         (cells-colers (terrain-cells-colers sekai)))
    (iter-matrix
     terrains
     #'(lambda (y x matrix)
         matrix
         (draw-cameraed-aabb* pane camera space-scale
                              (make-aabb :left (float (- (* x cell-width) (/ cell-width 2)))
                                         :right (float (+ (* x cell-width) (/ cell-width 2)))
                                         :top (float (- (* y cell-height) (/ cell-height 2)))
                                         :bottom (float (+ (* y cell-height) (/ cell-height 2))))
                              :ink (aref cells-colers y x))))))


;;; draw substances

(defun draw-substances (pane camera sekai num-grid)
  (let* ((space-scale (sekai-no-space-scale sekai))
         (uniformed-grid (sekai-no-uniformed-grid sekai))
         ;; 
         (substances-collision-to-camera
           (collisiones-to-camera camera uniformed-grid space-scale num-grid)))
    (loop for substance in substances-collision-to-camera
          do (let* ((collision-other-substance
                      (substance-collisioning substance))
                    (color (make-rgb-color
                            0.0 ;; (if collision-other-substance 0.5 0.0)
                            (if collision-other-substance 0.0 0.5)
                            0.0)))
               (draw-cameraed-substance
                pane camera space-scale substance :ink color

                :filled collision-other-substance)))))

;;; projection

(defun projection-of-camera (pane camera sekai num-grid
                             &key (terrains t) (substances t) (grid nil)
                             )
  (setf (camera-panel-height camera)
        (bounding-rectangle-height pane))
  (setf (camera-panel-width camera)
        (bounding-rectangle-width pane))
  (let ((space-scale (sekai-no-space-scale sekai)))
    (when terrains 
      (draw-terrains pane camera sekai))
    (when substances
      (draw-substances pane camera sekai num-grid))
    (when grid
      (draw-cameraed-grid pane camera num-grid space-scale))))

;;; project selecting lifes

(defun projection-of-selecting-the-lifes (pane camera sekai select-lifes selecting-the-life)
  (let* ((space-scale (sekai-no-space-scale sekai))
         (uniformed-grid (sekai-no-uniformed-grid sekai))
         ;;
         (selecting-lifes? (select-lifes-selecting-p select-lifes))
         (collisioes-to-selecting
           (if selecting-lifes?
               (aabb--uniformed-grid--collision-detection
                (cameraed-panel-aabb-to-sekai-aabb (select-lifes-panels-aabb select-lifes)
                                                   camera space-scale)
                uniformed-grid space-scale *num-grid*)
               (select-lifes-selectings select-lifes))))
    ;;
    (mapcar #'(lambda (sub)
                (draw-cameraed-substance pane camera space-scale sub 
                                         :filled t :ink (make-rgb-color 0.5 0.8 0.3)))
            collisioes-to-selecting)
    ;;
    (when selecting-lifes?
        (draw-aabb pane (select-lifes-panels-aabb select-lifes)  :ink +red+ :filled nil))
    ;;
    (when (typep selecting-the-life 'substance)
      (draw-cameraed-aabb* pane camera space-scale (substance-aabb selecting-the-life)
                           :ink +black+ :filled nil))))

;;; project the sekai


(defun projection-the-sekai (pane sekai)
  ;; vanish the pasts
  (with-bounding-rectangle* (x0 y0 x1 y1) (sheet-region pane)
    (draw-rectangle* pane x0 y0 x1 y1 :filled t :ink (background-color pane)))
    
  ;; project-substances
  (let* ((camera! (frame-camera (pane-frame pane)))
         ;;(space-scale (sekai-space-scale sekai))
         ;;(uniformed-grid (sekai-uniformed-grid sekai))
         ;;
         (select-lifes (frame-select-lifes (pane-frame pane)))
         (selecting-the-life (frame-selecting-the-life (pane-frame pane)))
         ;;(selecting-lifes? (select-lifes-selecting-p select-lifes))
         (current-simulation *current-simulation*)
         )
    ;; (projection-of-camera pane camera! sekai *num-grid*)
    (case current-simulation
      ((:phototaxis :recents)
       (projection-of-camera pane camera! sekai *num-grid*))
      ((:substances)
       (projection-of-camera pane camera! sekai *num-grid*
                             :terrains nil :grid t))
      ((:terrains)
       (projection-of-camera pane camera! sekai *num-grid*
                             :substances nil)))
    ;;
    (projection-of-selecting-the-lifes pane camera! sekai select-lifes selecting-the-life)
    ))

