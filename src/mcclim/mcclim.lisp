(in-package :kotonoha_mikoto)



;; sekai-projection-panel

(defclass sekai-projection-pane
    ;;(standard-extended-input-stream ; must have precedence over basic-pane
    ;;basic-pane
    ;;clime:always-repaint-background-mixin
    ;;permanent-medium-sheet-output-mixin)
    (clime:never-repaint-background-mixin basic-gadget
                                          )
  ((%background :initform (make-rgb-color 0.95 0.99 0.92) :reader background-color)))

(defmethod initialize-instance :after ((pane sekai-projection-pane) &rest args)
  (declare (ignore args)))


(defmethod compose-space ((pane sekai-projection-pane) &key width height)
  (declare (ignore width height))
  ;; Hmm. How does one constrain the aspect ratio of a pane?
  (make-space-requirement))


;;; Mcclim application-frame

(defparameter *current-simulation* :phototaxis)

(define-application-frame repl-of-projection-of-sekai  ()
  ((current-sekai :initform (sekai-no-hazimari *current-simulation*) :accessor frame-current-sekai)
   (camera :initform (make-camera) :accessor frame-camera)
   (mode-of-update-sekai :initform :stop :accessor frame-mode-of-update-sekai)
   (bottom-line :initform nil :accessor frame-bottom-line)
   ;;
   (dragging :initform nil :accessor frame-dragging)
   (select-lifes :initform (make-select-lifes) :accessor frame-select-lifes)
   (selecting-the-life :initform nil :accessor frame-selecting-the-life)
   )

  (:menu-bar t)
  (:panes
   ;; *
   (sekai (make-pane 'sekai-projection-pane))
   (stop :push-button :label "stop"
         :activate-callback #'(lambda (gadget) gadget 
                                (setf (frame-mode-of-update-sekai (gadget-client gadget)) :stop)))
   (start :push-button :label "start"
          :activate-callback #'(lambda (gadget) gadget
                                 (setf (frame-mode-of-update-sekai (gadget-client gadget)) :auto)))
   (step :push-button :label "step"
         :activate-callback #'(lambda (gadget) gadget
                                (setf (frame-mode-of-update-sekai (gadget-client gadget)) :step)
                                (update-application-parameters (pane-frame gadget))))
   (interactor :interactor)
   (bottom-line :text-field :value "kotonoha_mikoto" :editable-p nil)
   ;; sekai-status
   (sekai-status-minimap (make-pane 'sekai-status-minimap-pane))
   (sekai-status-texts :text-editor :value "aaaaaa" :nlines 12)
   ;; substances
   (substances-status-list 
    :list-pane :items '() :value-changed-callback #'substances-status-list-update-selection)
   (substances-status-text :application)
   (substances-status-show-following-map?
    (with-radio-box ()
      (make-pane 'toggle-button :label "follow" :id :follow)
      (make-pane 'toggle-button :label "recess" :id :recess)))
   (iketoshimono-status-frame-call
    :push-button :label "iketochimono" :activate-callback #'call-iketoshimono-application)
   (substances-status-camera-recenter
    :push-button :label "recenter" :activate-callback #'substances-status-camera-recenter)
   (substances-status-following-map (make-pane :application))
   )
  
  (:layouts
    (default 
        (vertically ()
          (:fill
           (with-tab-layout ('tab-page :name 'application-tabs)
             ("sekai" sekai)))
          (1/4 
           (horizontally ()
             (vertically () stop start step)
             (:fill
              (with-tab-layout ('tab-page :name 'status-tabs)
                ("sekai-status" 
                 (horizontally () sekai-status-texts sekai-status-minimap))
                ("substances"
                 (horizontally ()
                   (scrolling () substances-status-list)
                   substances-status-text
                   (vertically ()
                     iketoshimono-status-frame-call
                     substances-status-camera-recenter
                     (labelling (:label "follow?") substances-status-show-following-map?))
                   substances-status-following-map))
                ("interactor"
                 (horizontally () interactor))))))
          bottom-line))))


;;; iketoshimono-frame

(defun sane-find-pane-named (frame name)
  (map-over-sheets #'(lambda (p)
                       (when (string-equal name (pane-name p))
                         (return-from sane-find-pane-named p)))
                   (frame-panes frame)))

(defun format-iketoshimono-text (iketoshimono)
  (format nil " substance:~%~A~%~% enviroment-parameters:~%~A~%~% data-and-program:~%~A~%~%"
          (major-info-text-of-substance (iketoshimono-no-substance iketoshimono))
          (iketoshimono-no-enviroment-parameters iketoshimono)
          (iketoshimono-no-data-and-program iketoshimono)))


(defun add-extra-pane (frame title iketoshimono)
  (let ((fm (frame-manager frame)))
    (with-look-and-feel-realization (fm frame)
      (add-page (make-instance 
                 'tab-page
                 :title (format nil "~A" title)
                 :pane (make-pane 'text-editor-pane
                                  :value (format-iketoshimono-text iketoshimono)))
                (sane-find-pane-named frame 'application-tabs)
                t))))

(defun call-iketoshimono-application (gadget)
  (let ((selecting-substance (frame-selecting-the-life (pane-frame gadget))))
    (when selecting-substance
      (setf (frame-mode-of-update-sekai (gadget-client gadget)) :stop)
      (let (;;(selecting-the-life (frame-selecting-the-life (pane-frame gadget)))
            (selecting-the-life
              (find selecting-substance
                    (sekai-no-ikitoshi-ikerumono (frame-current-sekai (pane-frame gadget)))
                    :test #'(lambda (sub ike)
                              (substance-key-equal sub (iketoshimono-no-substance ike))))))
        (add-extra-pane (pane-frame gadget)
                        (substance-unique-key (iketoshimono-no-substance selecting-the-life))
                        selecting-the-life)))))


;; format to interractor

(defun format-to-interactor (control-string &rest format-arguments)
  (when (typep *application-frame* 'repl-of-projection-of-sekai)
    (let ((interactor-stream (get-frame-pane *application-frame* 'interactor)))
      (apply #'format interactor-stream control-string format-arguments))))


(defun format-to-bottom-line (control-string &rest format-arguments)
  (when (typep *application-frame* 'repl-of-projection-of-sekai)
    (setf (frame-bottom-line *application-frame*)
          (find-pane-named *application-frame* 'bottom-line))
    (setf (gadget-value (frame-bottom-line *application-frame*))
          (apply #'format nil control-string format-arguments))
  ))


;; Call System update by Mcclim-scheduled threading

(defun real-time-counter (internal-real-time-a internal-real-time-b)
  (/ (- internal-real-time-b internal-real-time-a) internal-time-units-per-second))


(defparameter *update-per-second-ideal* 30.0)
(defparameter *update-per-second-spend* 0.00)

(defparameter *current-updated-of-client* 0)


(defclass event-of-schedule-update-sekai (window-manager-event)
  ((val :initarg :val :accessor val)))

(defun sekai-update-schedule (frame the-real-time-start-the-current-event)
  (climb::schedule-event
   (frame-top-level-sheet frame)
   (make-instance 'event-of-schedule-update-sekai
                  :sheet frame
                  :val nil)
   (let* ((the-real-time-of-now (get-internal-real-time))
          (the-real-time-to-spend-the-event 
            (real-time-counter the-real-time-start-the-current-event the-real-time-of-now))
          (the-delay (max 0 
                          (- (/ 1 *update-per-second-ideal*) the-real-time-to-spend-the-event)
                          )))
     (setf *update-per-second-spend* the-real-time-to-spend-the-event)
     the-delay)
   ))


(defmethod handle-event ((frame repl-of-projection-of-sekai) (event event-of-schedule-update-sekai))
  (let ((the-real-time-start-event (get-internal-real-time))
        (frame-update-mode (frame-mode-of-update-sekai frame)))
    (when (eq frame-update-mode :auto)
      (update-application-parameters frame))
    (when (member (frame-state frame) '(:enabled :shrunk)) ;; sometimes stops frame update schedule?
      (sekai-update-schedule frame the-real-time-start-event))))


;; Update sekai and system

(defun update-sekai-system-of-client! (frame)
  (setf (frame-current-sekai frame)
        (update-sekai
         nil
         (frame-current-sekai frame)
         *current-simulation*)))

(defun update-status-interfaces-of-tab (frame)
  (let* ((current-tab
           (tab-layout-enabled-page (find-pane-named frame 'status-tabs)))
         (current-tab-title
           (tab-page-title current-tab)))
    (cond ((equalp current-tab-title "sekai-status")
           (update-sekai-status-frame-interface frame))
          ((equalp current-tab-title "substances") 
           (update-substances-frame-interface frame))
          ((equalp current-tab-title "interactor") 
           (identity nil)))))

(defun update-application-parameters-panes (frame)
  ;; projection to sekai panel
  (let ((sekai-projection-pane (find-pane-named frame 'sekai)))
    (repaint-sheet sekai-projection-pane (sheet-region sekai-projection-pane))
    )
  ;; bottom-line
  (if (= 0 (mod *current-updated-of-client* 10))
      (format-to-bottom-line
       "(update/second- ideal : spend = ~2,4f:~3,4f). Collisioon Detection: ~A"
       *update-per-second-ideal* (ignore-errors (/ 1 *update-per-second-spend*))
       *num-collision-detection*))

  ;; status tabed panel
  (update-status-interfaces-of-tab frame)
  )

(defun update-application-parameters (frame)
  ;; update sekai
  (update-sekai-system-of-client! frame)
  ;; values
  (incf *current-updated-of-client*)
  ;; panels
  (update-application-parameters-panes frame)
  
  ;; reset values
  (setf *num-collision-detection* 0))


(defmethod handle-repaint ((pane sekai-projection-pane) region)
  (with-bounding-rectangle* (x0 y0 x1 y1) (sheet-region pane)
    (climi::with-double-buffering ((pane x0 y0 x1 y1) (wtf-wtf-wtf))
      (declare (ignore wtf-wtf-wtf))
      (projection-the-sekai pane (frame-current-sekai (gadget-client pane))))))



;; run mcclim application

(defmethod run-frame-top-level :before ((frame repl-of-projection-of-sekai) &key)
  (let* ((sheet (frame-top-level-sheet frame))
         (schedule-updating-event (make-instance 'event-of-schedule-update-sekai :sheet frame)))
    (queue-event sheet schedule-updating-event)
    ))

(defun run-mcclim-application ()
  (run-frame-top-level (make-application-frame 'repl-of-projection-of-sekai)))

