
(in-package :kotonoha_mikoto)

;;;; related to projection-region.lisp

;;; camera

(defun track-move-camera (pane camera! start-y start-x)
  (let* ((last-x start-x)
         (last-y start-y))
    (block camera-moving
      (tracking-pointer (pane)
        (:pointer-motion (&key y x)
         (let ((current-y y) (current-x x))
           (move-camera! camera! (- current-y last-y) (- current-x last-x)) 
           (setf last-x current-x last-y current-y)
           (update-application-parameters-panes (pane-frame pane))))
        (:pointer-button-release (&key event)
         (when (eql (pointer-event-button event) +pointer-middle-button+)
           (return-from camera-moving)))))))


;;; select-lifes

(defun track-select-lifes (pane select-lifes! camera! start-y start-x)
  (setf (select-lifes-panels-aabb select-lifes!) 
        (make-aabb :left (float start-x) :right (float start-x)
                   :top (float start-y) :bottom (float start-y)))
  (setf (select-lifes-selecting-p select-lifes!) t)
  (block block-select-lifes
    (tracking-pointer (pane)
      (:pointer-motion (&key y x)
       (setf (select-lifes-panels-aabb select-lifes!)
             (make-aabb :left (min (float start-x) (float x)) :right (max (float start-x) (float x))
                        :top (min (float start-y) (float y)) :bottom (max (float start-y) (float y))))
       (update-application-parameters-panes (pane-frame pane)))
      (:pointer-button-release (&key event)
       (when (eql (pointer-event-button event) +pointer-left-button+)
         (with-slots (space-scale uniformed-grid) (frame-current-sekai (pane-frame pane))
           (setf (select-lifes-selectings select-lifes!)
                 (aabb--uniformed-grid--collision-detection
                  (cameraed-panel-aabb-to-sekai-aabb (select-lifes-panels-aabb select-lifes!)
                                                     camera! space-scale)
                  uniformed-grid space-scale *num-grid*))
           (format-selected-lifes-to-interactor select-lifes!))
         (setf (select-lifes-selecting-p select-lifes!) nil)
         (update-application-parameters-panes (pane-frame pane))
         (return-from block-select-lifes)))))
  (setf (select-lifes-selecting-p select-lifes!) nil))

(defun format-selected-lifes-to-interactor (select-lifes!)
    (format-to-interactor
     "selected : ~A~%"
     (mapcar #'(lambda (sub)
                 (with-slots (coordinate radius collisioning (key unique-key)) sub
                   (format nil "key: ~A, ~A, length: ~A , colling:~A"
                           key coordinate radius (length collisioning))))
      (select-lifes-selectings select-lifes!))))


;;; pointer mouse

(defmethod handle-event ((pane sekai-projection-pane) (event pointer-motion-event))
  )

(defmethod handle-event ((pane sekai-projection-pane) (event pointer-button-press-event))
  (let ((pressed-button (pointer-event-button event))
        (camera! (frame-camera (pane-frame pane)))
        (select-lifes! (frame-select-lifes (pane-frame pane))))
    (when (equal pressed-button +pointer-middle-button+)
      (track-move-camera pane camera! (pointer-event-y event) (pointer-event-x event)))
    (when (equal pressed-button +pointer-left-button+)
      (track-select-lifes pane select-lifes! camera! (pointer-event-y event) (pointer-event-x event)))
    ))

(defmethod handle-event ((pane sekai-projection-pane) (event pointer-button-release-event))
  (let ((released-button (pointer-event-button event))
        (camera! (frame-camera (pane-frame pane))))
    (when (equal released-button +pointer-wheel-up+)
      (magnificate-camera! camera! :expansion)
      (update-application-parameters-panes (pane-frame pane)))
    (when (equal released-button +pointer-wheel-down+)
      (magnificate-camera! camera! :reduction)
      (update-application-parameters-panes (pane-frame pane)))))


(defmethod handle-event ((pane sekai-projection-pane) (event pointer-exit-event))
  (setf (frame-dragging (pane-frame pane)) nil))
