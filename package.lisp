(in-package :cl-user)

(defpackage kotonoha_mikoto
  (:use #:cl-user
        #:clim
        #:clim-lisp
        #:clim-tab-layout
        #:bordeaux-threads))