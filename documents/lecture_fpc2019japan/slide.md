
count: true

layout: true

---

class: center, middle


# 試験管の中の人工生命体
### Artificial-Life in the Solvent 


### <ruby><rb>池上 蒔典</rb> <rt>いけがみ まきのり</rt></ruby> (Makinori Ikegami)

---

# 次第


1. 人工生命とは何か
1. 帰納的学習
1. 自己複製
1. シミュレータ
1. 妄想


???

この講演では､

ならば幸いです｡

人工生命の歴史を追っていく


帰納的学習たる経験した事象から共通の法則性を導き出す発見であって､述語演算の始まりであった｡

思考は来たるべき未来に備えるべく生存の戦略であった｡


---

class: center, middle

# 人工生命

<br /><br />

人工生命とは何か?

???

人工生命とは何か｡

- 人が創った生命?
読んで字のごとく､人が創った生命であると思われると思われるが､生命とは何か?という疑問が湧いてくる｡


---

class: center, middle

# 人工生命

<br /><br />

生命を表現して､　 　 <br /> 生命を発見する､姿勢｡

???

- 私が思う､人工生命とは何か?というのは､  
生命の性質や振る舞を､(仮説でも､それっぽいものでも.)何らかの空間に表現して､ 生命の性質や生命の振る舞いを､  
新たに発見していく､その様な姿勢や取り組みであると､(不肖の身だが)最近は感じている｡

---

class: center, middle

### Self Reproduction and Inductive learning
# 自己複製 と 帰納学習


???

生命を生命たらしめる要因とは? (what let life life?)

ひとつには､来たるべき未来に備て思考すること｡ (学習)
ひとつには､全て死なないべく･種を残すべく､自らを残す｡ (複製)

---

# 帰納学習 (Inductive-learning)

- 経験から共通する法則を発見すること｡

<center>
<img src="data/drawio/kinou_learning.png"></img>
</center>

???

- 思考

生命について考えると､賢さ-破滅に対する理に適った反応が備わっている(条件反射や思考)｡

- 帰納的学習とは､経験から共通する法則を発見すること｡

1. 死なないならEから逃げる｡
1. Aは焼け死んだし､Bも焼け死んだし､Cも爆死した｡
1. 死なないなら火から逃げる｡



---

# 帰納学習 (Inductive-learning)

- 事実に共通する事実を導き出すこと｡
- 式に共通する式を解くこと｡

<br /><br /><br />

- 決定問題
```
(F a) => (Just x)
```

- 非決定問題
```
(F a) => (Just p) | (maybe [p]) | (fail)
```


???

- 非決定性問題 行き先が不定｡  
評価するとどこかで矛盾したり､値が定まらなかったり､｡する｡のうちの､

- 記憶(事実)表現する示す式に共通する式が｡ 一意に解決された式(どの入力に対しても矛盾しない式｡)が､  
帰納学習を表現する､表現されるのではないか､､､｡(仮説)


- ミドリムシの頃から､Prologの様な論理式のソルバを持っていたのではないかと思っ､､､妄想している｡


---

# 自己複製 (Self Reproduction)

### Von Neumann universal constructor  

![https://upload.wikimedia.org/wikipedia/commons/c/c4/Nobili_Pesavento_2reps.png](data/Nobili_Pesavento_2reps.png)

#### セル・オートマトンによる自分自身を印刷する機械｡
#### - Von Neumann universal constructor - Wikipedia(https://en.wikipedia.org/wiki/Von_Neumann_universal_constructor)
#### - ライフゲームの世界９ 最終回【複雑系】(https://www.nicovideo.jp/watch/sm19566296).


???

- ロボットは自分で自分を作り出すことは可能か? に答えうる｡

- 1940年代 (50年代(?))  にノイマン氏の頭の中だけで組み立てられた｡
- いきなり現実の空間に創るのは難しかったため､ 頭の中に物理法則を作成(抽象的なモデルを作成)｡
- 格子状の世界を用意し､各要素(セル)はそのセルと上下左右の状態によって､29種類の状態を遷移するようにした(Von Neumann Universal constractor)
- 頭の中のこのモデルが､この頭の中の物理法則の中で､自己複製をしうることを証明した｡
- コンピュータの無い時代にである｡


---

# 自己複製 (Self Reproduction)

### Quine
自分自身を出力するプログラム

- common lisp
``` commonlisp
((lambda (s) (print (list s (list 'quote s))))
 '(lambda (s) (print (list s (list 'quote s)))))
```

- haskell
```haskell
(putStrLn . \s -> s ++ show s) "(putStrLn . \\s -> s ++ show s) "
```

- python
```python
w = "print('w = ' + chr(34) + w + chr(34) + chr(10) + w)"
print('w = ' + chr(34) + w + chr(34) + chr(10) + w) 
```

#### - Quine - Rosetta Code (https://rosettacode.org/wiki/Quine).

???

自分自信のソースコードと等しいソースコードを表示する｡  
ただし､atomを評価するだけだったり､ファイルシステムから自分のファイルを読み込んだりするのはルール違反｡

それぞれ､データを示す節と､プログラムを示す節があり､  
プログラムを示す節には､プログラムを示す節とデータを示す節が書かれ､  
データを示す節では､プログラムを示す節が書かれる｡  

まるで構造としての細胞と､データとしての遺伝子の如くである｡


---

# 現実世界の自己複製

- 決定的なコンピュータの､データは破壊されない｡
- 現実の物理環境の､反応や構造は破壊される｡

???

自己複製プログラムを､現実世界にあてはめて考えると､｡

- 確かに出力されるのは自分自身だが､理想的な体系(プログラムの意図の通りに動く)の中の話であって､実際の物理環境にはデータを破壊する要因がある
- (構造が似た分子による反応(ガン)､熱に依る性質の変化､放射能や宇宙線による破壊､､など､､､｡)｡

自己複製をしたがために､構造が破壊されるリスク(確率)が低下し､結果として地球に生き残っている｡

- ダーウィン遺伝的アルゴリズムは､コンピュータの中で破壊をシミュレートしている｡


- そもそも､自分達を破壊しうる未来においても生き残り続けるべく､自分達を書き換え続けるる自分達を書き続けてきたのが､ 自分達生命である｡

- computer-simulation

---


# コンピュータ･シミュレーション

- 物理シミュレーション  
- 機械学習･人工知能  

<center>
... 世界を通した生命のシミュレーション ... (?)
</center>

##### 　 

<center>
<img src="data/izumi.png" height="180" class="smaller"> </img>
<img src="data/game-theory-simu.png" height="180" class="smaller"> </img>
</center>

<center>
<img src="data/coin_gather.png" height="180" class="smaller"> </img>
<img src="data/idea/kami2.jpg" height="180" class="smaller" rotate="90"> </img>
</center>

##### <center> - 反応拡散系シミュレーション (https://gitlab.com/i-makinori/izumi) - ゲーム理論シミュレーション (https://github.com/i-makinori/game-theory-simulator) </center>
##### <center> - ゲームの中のニューラルネットAI (https://gitlab.com/i-makinori/coin_gather_game) - そして最後の神頼み (奈良県は橿原神宮にて撮影) </center>

???

- 物理シミュレーション｡ 数理の言語(数値や数値の変化に着目した)｡
- 機械学習･人工知能｡ どうでほしいか(目標)を表現できれば､それっぽく作ってもらえる｡

- 生命を通した世界のシミュレーションは､世にあまり出ていない｡
- 生命とは何であるかを考える姿勢や､生命を表現する姿勢は､一時は流行したものの､当時に対しては現在は下火になっている｡
- 論理学的な式と生命を合わせて考える手法が､下火になっている｡


---

# Tierra

仮想コンピュータ上にデジタル生態系を創造｡  
生息するデジタル生物たちは淘汰を乗り越えて多様に進化していく｡

![http://life.ou.edu/pic/almonda3.jpg](data/almonda3.jpg)
![http://life.ou.edu/pic/almondc3.jpg](data/almondc3.jpg)

#### 左図 祖先種(赤線)が大半を占めている｡ パラサイト(黄線)はまだ少ない｡ 右図 パラサイトが増殖し祖先種が急減｡ 免疫種(青線)が現れ始める｡
#### - Tierra Photoessay (http://life.ou.edu/pubs/images/). Tierra home page (http://life.ou.edu/tierra/index.html).
#### - Tierra入門 (http://web.archive.org/web/20001202002200/www.hip.atr.co.jp/~kim/TIERRA/tierra.html).

???

(小生が現在知る中で､)最も現実世界の生命に則したシミュレーション｡

進化生物学者 Thomas S.Ray博士が開発｡
仮想コンピュータ上にデジタル生態系を創造し、そこに生息するデジタル生物たちは自然淘汰を乗り越え多様に進化していく｡

現実の地球での生命たちは資源を奪い合っていると言えるが､
コンピュータの資源は､メモリと計算｡

単純なプログラムから始まり､様々な種類のプロセスが発生した｡

- 原本: Ray, T. S. 1992. Evolution, ecology and optimization of digital organisms. Santa Fe Institute working paper 92-08-042. 


---

class: center, middle

# 妄想


---

# 世界シミュレーションの作成

- 距離の概念
- マルチ･ エージェント･シミュレーション (MAS)
- 超並列仮想コンピュータ


???

- 位置や依る環境の変化｡  コンピュータのメモリは距離の違いが無い(メモリ番地へのポインタ)｡
- 自ら動く物を大量に導入｡ エージェントの相互作用､進化､多様性｡ 
- 独自の命令セットを持つ､超並列仮想コンピュータと､そのプロセスとしての生命｡

これらの概念で､物理学を､生命と生命単位の理解を､  
生命と生命や 生命と環境が 相互に作用する､  
多様性のある､  
生命の世界のシミュレーションが出来るのではないか?

---

class: middle, center

# 世界シミュレーション
### 誠意(?)製作中

---

# 実装完了と未実装

---


# 退屈なること実装するも
- 距離の概念
- 光と水
- マルチエージェントシステム
  - 走光性

---

# 肝心なること実装に至らず

- 超並列仮想コンピュータ
  - 計算コスト
  物理実行時間か､関数単位のコストか｡
  - 並列性  
  1000億の粒､100億の命｡
  - Lisp処理系  
  独自の関数セット
  - 中断と継続

- 生命としてのプログラム
  - 生命が可能な関数  
  自身と環境や､自身と自身の作用｡

- 最初の生命
簡単な条件反射と自己複製

---

# 観測しようと箱ぞ無し

- 記憶機能  
観測した事実を記録
- 学習機能 - 帰納的学習  
事実と事実達からの事実
- 生命の中の生命
- 相互作用
  - 相互作用
  生命と生命の相互作用､生命と環境
  - 多様性
- 進化
- 猫の死亡  
<s> 毒ガスと放射能で 猫を消毒だぁ~｡ ヒャハァァァァァァァァァァァァァァァァァァァァ </s>  
隕石や <s> ウイルスで人類を滅ぼそう｡ </s>  
生けとし生けるものに試練を課そう｡  


???

されど思考に及ばぬうつつ｡
- 自己言及､こいつが不要､ホトトギス
- 距離 - 曲面座標系の空間

---

class: middle,center

# 製作中のシミュレータ

![](data/drawio/simulator.png)

---

# ユーグリッド空間

- ユーグリット空間
- 当たり判定
- 当たり判定高速化

???

実装済み

実演

```commonlisp
(ql:quickload :kotonoha_mikoto)
(kotonoha_mikoto::app-main :substances)
```

---

# 地形と資源

光と水 
- 供給 - 昼と夜｡ 地軸の傾き｡
- 拡散 - 反応拡散系の拡散項｡
- 放射 - 夜の放射冷却｡冬の寒さ｡

???

実装済み

- 実演

```commonlisp
(kotonoha_mikoto::app-main :terrains)
```

---

# 地形と資源

光と水に加えて､､､｡

物質･式
- 供給 - 生命の生成と放出
- 拡散 - 生命の移動と放出
- 分解 - 半減期による光と水への還元

???

未実装･思考中

---

# マルチ･エージェント･シミュレーション

走光性 
- 光に向けて動く性質
- それぞれの個体は光に向けて移動する

???

実装済み

- 実演

```commonlisp
(kotonoha_mikoto::app-main :phototaxis)
```

---

# シミュレータの中の生命｡

- 自己複製をする遺伝的プログラミング

???

- 評価される式
-> プログラム､ 細胞全体､ 作用する物理的な構造物､コンピュータ
- Quoteされた式は評価されない(処理系依存)
-> データ､細胞の中の遺伝子､ 静かな物理的構造物(作用するけどね)､ ハードディスク
 

---

# 遺伝的､､､

1. 遺伝的アルゴリズム
```commonlisp
#1A(2 4 1 2 3 4 1 2 1 1 1 3 4 4 ... )
```
1. 遺伝的プログラミング
```commonlisp
'(do (something) (earned (it (in (long long (very (long long long))) (list evolution)))))
```

???

- 遺伝的アルゴリズム
- 生命としては意味の無いパラメータを決定する
- ダーウィンの進化論のみを基にするため､確率に支配され､生命ごとに意思は存在し得ない｡  
  理由もなく生き残ってしまう｡

- 遺伝的プログラミング
- 行動の理由を言葉として理解できるようになる(順方向ニューラルネットワークに対する比較すべきな点)
- 一方で､エージェントがエージェントをどのように産むか?は


---

### MASと､自己複製をする遺伝的プログラミングで､ <br /> 人工生命が発生するのではないか?


1. 自己複製プログラム (Quine)
```commonlisp
((LAMBDA (S) (LIST S (LIST 'QUOTE S)))
 '(LAMBDA (S) (LIST S (LIST 'QUOTE S))))
```
1. 自己複製プログラムと他の作用
```commonlisp
((LAMBDA (S) (LIST (CADR S) (LIST 'QUOTE S)))
 '(LIST (LAMBDA (S) (LIST (CADR S) (LIST 'QUOTE S))) 'THING1 'THING2))
```
1. 学習機能付き複製プログラム
```commonlisp
((LAMBDA (S) (..self-reproduct.. (CADR S) (LIST 'QUOTE S) ..)) ;; 学習と自己複製
 '(LIST
   (LAMBDA (S) (..self-reproduct.. (CADR S) (LIST 'QUOTE S) ..)) ;; 学習と自己複製のソースコード
   'THING1 'THING2))
```

???

- 自己複製プログラムと他の作用
触媒反応系のようなもの(?)

- ミュレーションに適した形のQuineを探したい(最初の自己複製を示すlambdaがカッコ悪い)｡


---


# 世界の時 生命の時

- 宇宙更新一回につき､生命の数だけ生命更新
- 生命更新では生命のプログラムの実行


???

未実装(?)

- 生命更新一回
- 遺伝的プログラミングの実

---

# 生命とプロセス 

- 実行時間-離散時間と更新､ ユーグリッド空間との対応 ､ マルチプロセス-超並列仮想コンピュータ､ (相互作用)

???

未実装

---

# プロセスと計算の凍結

- 単位時間の計算量は限られている｡
- 超並列仮想コンピュータの中のプロセス(生命)は､何度でも計算を中断し､何度でも計算を再開したい｡

- 継続

???

- 継続 (未実装)
凍結 計算を途中で止める｡ (その時の環境メモリの保存)
継続 計算を再開する｡ (保存されたところから実行する)

継続は 理解及ばず(というのもあって) 利用せず

---

# 純Lisp

### Lispの中のLisp 


???

実験中

- 生命のプログラムを評価して､微笑時間での作用を表現したい｡

Tierraも仮想コンピュータと独自の命令セットを持つ


---

# 生命の機能

式の評価
- 観測 自分の周囲の環境を読み取る
- 移動 ユーグリッド空間を移動する
- 吸収 環境から光や水や式を取り込む
- 生成 評価可能な式を内部に生成する
- 排出 生命内の式を環境に放出する
- データ構造を制御する関数

???

妄想

- $$$$$$$$$$$$$$$$$


# 実装済み

- 距離の概念
- 光と水
- マルチエージェントシステム
  - 走光性

---

# 実装していきたい､､､

### 超並列仮想コンピュータ

- 独自のLisp処理系
- 継続する生命プログラム
- 最初の生命


---

# 観測したい

- 記憶･学習
- 細胞内小器官
- 相互作用･多様性･進化
- 試練の時代


---

# 質疑･議論

- 帰納的学習
- 自己複製
  - 火星人自己複製セルオートマトン
  - Quine
- Tierra
- 世界シミュレーション
  - 自己複製 - 確率的破壊から進化意識
  - 超並列仮想コンピュータ
  - 世界と生命

### <center> - 質疑･議論 - </center>
```commonlisp
(or (or (∀ questions) (∀ discuss))
```

???


超並列仮想コンピュータの中のプロセス(生命が)､自己複製し･学び･相互作用し合う中で･進化を遂げていく､  
この様なシミュレーションが完成しようとも､  

または現在の他の人工生命の諸分野(カオス･フラクタル･ライフゲームなど)を知ろうとも､  

生命･人工生命という深淵の理解には遠く及ばないであろう｡  

生命とは何かの､愚生の思考もが､  
何らかの発見の､足がかりになれば､  
この発表が､何か皆様の心に残るものであれば､  
この発表が､何かの疑問を生じ得るものであったのであれば､  
幸いである｡




