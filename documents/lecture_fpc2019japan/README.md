

# 講演資料 試験管の中の人工生命体(Artificial-Life in the Solvent)

関数型プログラミングカンファレンス2019 in Japan

- https://fpc2019japan-event.peatix.com/
- https://twitter.com/Fpc2019Japan

### presentation slides

the system of presentating slide uses [remark](https://github.com/remarkjs/remark).


- `slide.md` is manuscript of presentation.

- to indicate lecture slide and memos, 


run server in localhost

```
$ ./start_server.sh
```

or

```
$ pyton -m http.server
```

and access to [http://localhost:8000/slide.html#p1](http://localhost:8000/slide.html#p1) 





### 会議を終えての感想


##### 感想原稿

- SNS

```

池上 蒔典(Makinori Ikegami) です
試験管の中の人工生命体(Artificial-Life in the Solvent)
の題にて講演を致しました｡


散発的で纏まりなき､解りにくい講演だったと感じます｡
小生は愚拙の懈怠を憶えましたが､永遠に解らなそうな問題を永遠に解らないなりにも公表することが出来て､十分に有意義な時間であったように感知します｡
纏まりなき中でも主張を汲み上げてしこうをした皆様に於きまして､この時間が､未知への疑問を生じえるものであったのであれば､尚､うれしく思われるかと存じます｡


ソースコードは､ https://gitlab.com/i-makinori/kotonoha_mikoto にて入手可能で､
発表の資料は､上リポジトリの､ documents/lecture_fpc2019japan/ にあります｡
スライドを表示する場合は､資料を参照の如く､cloneし､サーバーを立て､ブラウザから slide.html#p1 に接続します｡

```



