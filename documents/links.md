
# リンク集


| 名前                                 | 作成者                                    | リンク                                                                               | コメント                                                            |
|--------------------------------------|-------------------------------------------|--------------------------------------------------------------------------------------|---------------------------------------------------------------------|
| 人工生命を創ってみた                 | Ryu                                       | https://www.nicovideo.jp/mylist/11205593                                             | 暇なので人工生命を創ってみた 動画の連載                                        |
| 生態系シミュレータを作ってみた       | GreenTea                                  | https://www.nicovideo.jp/mylist/30318601                                             | 生態系シミュレータを作成していく動画の連載                          |
| Vendian                              | Douwe Osinga                              | http://www.osinga.com/projects/vendian                                               | アセンブラの如きプログラムが遺伝される、人工生命シミュレータ        |
| Tierra                               | Simon Fraser                              | http://life.ou.edu/tierra/                                                           | 仮想の計算機の上に生命としてのプログラム                            |
| ライフゲームの世界                   | せがわ（旧はむくん）                      | https://www.nicovideo.jp/mylist/34610498                                             | ライフゲームを解説する動画の連載                                    |
| VonNeumann'sSelf-ReproducingAutomata | 発見: Jhon VonNeumann, 編纂:ARTHURW.BURKS | http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.1029.4814&rep=rep1&type=pdf | 自己複製セル・オートマトン                                          |
| izumi                                | 池上蒔典                                  | https://gitlab.com/i-makinori/izumi                                                  | 反応核酸系のシミュレータを作ろうとしたもの｡ 結局はセルオートマトン｡ |
| game-theory-simulator                | 池上蒔典                                  | https://github.com/i-makinori/game-theory-simulator                                  | ゲーム理論のシミュレータです｡ セルが近傍のセルごとに取引をします｡   |
| kotonoha_mikoto | 池上 蒔典 | https://gitlab.com/i-makinori/kotonoha_mikoto | 環境の変位に対する適合としての学習と､世代間の継承｡ 作成中 |

