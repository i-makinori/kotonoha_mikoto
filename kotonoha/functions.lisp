
(in-package :kotonoha)

;;; genetic-functions

;; lisp

(defun life-esse (life memoried-key)
  ;; 生きたる者の印｡ フレームごとのプログラムの開始地点
  )

(defun life-funcall (life memoried-key)
  ;; keyに対応するメモリの式を評価する
  ;; :continue を call すると､言語環境パラメータのみを復元して､直前フレームでの計算が再開される｡(?)
  ;; 継続で配列index無しで､断続的な実行時間の中で､実行時間の長い処理をする...?
  )


(defun life-if (life exist-or-fai exist fai)
  )


(defun life-cons (life formula1 formula2)
  )

(defun life-car (life formula)
  )

(defun life-cdr (life formula)
  )

(defun life-eq (life atom1 atom2)
  )

(defun life-atom (life formula)
  )

;; observe

(defun observe-self (life meoried-key)
  ;; 自身の自由メモリを参照する.  
  )

(defun observe-env (life memoried-key)
  ;; 環境を観測する. 自分の周辺の環境を見る. (key direction)をlisto
  (case memoried-key
    (:remain-unit-times-calculation 0) ;; 自身の残り計算量
    (:calculation-resource-map nil) ;; 周辺の計算資源の地図
    ((t) life nil)
  ))



;; write

(defun write-self (life memoried-key formula)
  ;; 自身の自由メモリに書き込む. life-funcall と合わせてdefun
  )

(defun discharge (life memoried-key) ;; dimension-key
  ;; 排出する=環境へ書き込む. 複製された自己の排出. 永続的でない共有された関数たりえる
  (case memorid-key
    ;; (:direction (cons left-right up-down))   ;; 移動する
    
  ))


;; environment

(defun conversion (life memorid-key)
  ;; memoried-keyに対応する式を分解する｡ 実質的な攻撃｡
  ;; 環境は､メモリ容量と式(無機物)と死までの計算量を得る｡
  ;; 自身にconscellの要素の式とmempried-keyが拡散される
  ;; プログラム開始地点が単一となるまでは､損傷したプログラムも生存し続ける｡
  )

(defun absorb (life meoried-key enviroment-memoried-key)
  ;; 吸収する. 自身のmemoried-keyに環境内のenviroment-memoried-keyに対応する無機物を代入する
  ;; 生きているものを取り込もうとしたら失敗
  )

;;; costs table

(defun genetic-cost-table ()
  ;; (function calcu-cost memory-cost)
  '((#'life-esse 0 0)
    (#'life-funcall 1 0)
    (#'life-if 4 0)
    (#'ife-cons 2 0)
    (#'life-car 2 0)
    (#'life-cdr 2 0)
    (#'life-eq 2 0)
    (#'life-atom 2 0)
    ;; observe
    (#'observe-self 1 0)
    (#'observe-env 2 0)
     ;; write
    (#'write-self 2 10) ;; length
    (#'discharge 2 -10) ;; length
    ;; environment
    (#'conversion 10 0)
    (#'absorb 1 10) ;; length of particle
    ))
