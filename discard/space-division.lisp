
(in-package :kotonoha_mikoto)

;; space division 4-tree
;; are changed to uniformed grid ... because of organizing thoughs.





;; morton space division -- binary version

(defparameter *morton-level* 4)


;; object for (liner(?)) quaternary tree

;; space length

(defun morton-space-length-of-level (level)
  (expt 4 level)
  )

(defun liner-quateranry-tree-length (level)
  ;; F(l) is all quateranry of l-leveled morton quaterany space. F(0)=>root
  ;; F(0) = 1 ,F(n) = (2^n)^2+F(n-1) # (n=expt)  ;; recurrence formula
  ;; F(l) = (Sigma (n from 0 to l) (2^n^2 = 4^n))   ;; to sigma ;; 4^n::4^n-separated-tree
  ;; Z(r k) = (Sigma (n form 0 to k) r^n) is (1-r^(k+1)) / (1-r) ;; Derive the Sigma formula
  ;; => F(l) = (1-4^(l+1)) / (1-4)
  (/ (- 1 (expt 4 (+ level 1)))
     (- 1 4)))

(defun liner-quaternary-tree-index (index-of-level morton-level)
  (+ index-of-level
     (liner-quateranry-tree-length (- morton-level 1))))



;; Liner Quaternary Tree
;; liner-tree : [0]|[1 1 1 1]|[2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2]|
;; index-corresponds (lvel=>length) : (0::root=>1) | (1=>4) | (2=>16)
;;
;; Ll(l) :: Level's space Length(level)  = (2^level)^2 = 4^level ;; only the level
;; Tl(l) :: Tree's space Length(level) = sigma(Ll(l)) = (1-4^(l+1)) / (1-4) ;; count all levels



(defparameter *liner-quaternary-tree*
  (make-array (liner-quateranry-tree-length *morton-level*)
              :initial-element nil))


(defun push-to-tree! (liner-tree index-of-level morton-level object)
  ;; O(n)
  (let ((tree-index
           (liner-quaternary-tree-index index-of-level morton-level)))
    (if (find object (aref liner-tree tree-index) :test #'eq)
        nil
        (setf (aref liner-tree tree-index)
              (cons object (aref liner-tree tree-index)))))
    liner-tree)

(defun remove-from-tree! (liner-tree index-of-level morton-level object)
  ;; O(n)
  (let ((tree-index
           (liner-quaternary-tree-index index-of-level morton-level)))
    (setf (aref liner-tree tree-index)
          (remove object (aref liner-tree tree-index)
                  :test #'eq)))
    liner-tree)

(defun object-move-tree!
    (liner-tree index-of-level-ago morton-level-ago index-of-level-next morton-level-next object)
  (if (and (= morton-level-ago morton-level-next) (= index-of-level-ago index-of-level-next))
      liner-tree
      (let* ((removed-tree!
               (remove-from-tree! liner-tree index-of-level-ago morton-level-ago object))
             (pushed-tree!
               (push-to-tree! removed-tree! index-of-level-next morton-level-next object)))
        pushed-tree!)))

;;;; apply to substances




#|

;; register and remove tree needs O(n)
;; want O(1) to tow-way list and pointers-of-index'es-liner-tree

(defstruct (object-for-tree)
  ;; trees-object
  (morton-level 0)
  (index-of-level 0)
  (object nil)
  (previous nil)
  (next nil))


;; pointers of first-registed-object of *liner-quaternary-tree* of index
;; pointers of index's *liner-quaternary-tree*'s first-registerd-object
(defparameter *pointers-of-first-object-of-liner-tree-of-index*
  (make-array (liner-quateranry-tree-length *morton-level*)
              :initial-element (make-object-for-tree)))


(defun reregister-trees-object-to-pointers-of-first-trees-object-of-index!
    (pointers-of-first-object-of-liner-tree-of-index object-for-tree)
  (let ((tree-index
           (liner-quaternary-tree-index 
            (object-for-tree-index-of-level object-for-tree)
            (object-for-tree-morton-level object-for-tree))))
    (setf (aref pointers-of-first-object-of-liner-tree-of-index tree-index)
          object-for-tree
          ))
  pointers-of-first-object-of-liner-tree-of-index)


(defun push-to-tree (pointers-of-first-object-of-liner-tree-of-index
                     morton-level index-of-level object)
  (let* ((first-same-indexes--trees-object
           (aref pointers-of-first-object-of-liner-tree-of-index
                 (liner-quaternary-tree-index index-of-level morton-level)))
         (previous-first-same-indexes--trees-object
           (object-for-tree-previous first-same-indexes--trees-object))
         (new-trees-object
           (make-object-for-tree
            :morton-level morton-level :index-of-level index-of-level
            :object object
            :previous previous-first-same-indexes--trees-object
            :next first-same-indexes--trees-object)))
))
|#












(defun show-binary-number (num)
  (format nil "#b~b" num))
  

(defun bit-separate32 (n)
  (let*
      (;; ___________________________  #b|xxx|xxx|xxx|xxx|xxx|xxx|xxx|xxx
       (n (logand (logior n (ash n 8)) #b00000000111111110000000011111111)) ;; #x00ff00ff))
       (n (logand (logior n (ash n 4)) #b00001111000011110000111100001111)) ;; #x0f0f0f0f))
       (n (logand (logior n (ash n 2)) #b00110011001100110011001100110011))
       (n (logand (logior n (ash n 1)) #b01010101010101010101010101010101)))
    n))

(defun get-2d-morton-number (y x)
  (logior (ash (bit-separate32 x) 0)
          (ash (bit-separate32 y) 1)))

















;;; morton space division -- list version

#|
(defun liner-quateranry-tree-length (level)
  ;; F(l) is all quateranry of l-leveled morton quaterany space. F(0)=>root
  ;; F(0) = 1 ,F(n) = (2^n)^2+F(n-1) # (n=expt)  ;; recurrence formula
  ;; F(l) = (Sigma (n from 0 to l) (2^n^2 = 4^n))   ;; to sigma ;; 4^n::4^n-separated-tree
  ;; Z(r k) = (Sigma (n form 0 to k) r^n) is (1-r^(k+1)) / (1-r) ;; Derive the Sigma formula
  ;; => F(l) = (1-4^(l+1)) / (1-4)
  (/ (- 1 (expt 4 (+ level 1)))
     (- 1 4)))
|#

(defparameter +upper-left+ 0) (defparameter +upper-right+ 1)
(defparameter +lower-left+ 2) (defparameter +lower-right+ 3)


(defparameter +a+ +upper-left+) (defparameter +b+ +upper-right+)
(defparameter +c+ +lower-left+) (defparameter +d+ +lower-right+)


(defun morton-index-of-line (point level length)
  (let* ((cell-length (/ length (expt 2 level)))
         (index-number (floor (/ point cell-length))))
    index-number))


(defun line-direction-to-direction (y-upper? x-left?)
  (+ (* 2 (if y-upper? 0 1)) (if x-left? 0 1))
)

(defun get-2d-morton-key (line-y-index line-x-index level)
  ;; todo : O(level) to O(1)
  (labels
      ((aux (num-y num-x list lev)
         ;; (format t "~A ~A ~A ~A~%" lev num-y num-x list)
         (if (<= lev 0)
             (cons nil list)
             (let* 
                 ((upper? (evenp num-y))
                  (left? (evenp num-x))
                  (direction (line-direction-to-direction upper? left?)))
               (aux (floor (/ num-y 2))
                    (floor (/ num-x 2))
                    (cons direction list)
                    (- lev 1))))))
                    
    ;; (format t "(~A=~B) (~A=~B) ~A => ~%" line-y-index line-y-index line-x-index line-x-index level)
    (aux line-y-index line-x-index '() level)))


(defun morton-index-of-plane (point-y point-x level length-y length-x)
  (let ((y-line-index (morton-index-of-line point-y level length-y))
        (x-line-index (morton-index-of-line point-x level length-x)))
    (get-2d-morton-key y-line-index x-line-index level)
    ))



(defun morton-index-of-space-tree (point0-y point0-x point1-y point1-x level length-y length-x)
  (let ((p0-morton-index (morton-index-of-plane point0-y point0-x level length-y length-x))
        (p1-morton-index (morton-index-of-plane point1-y point1-x level length-y length-x)))
    ;; (format t "p0: ~A, p1:~A ~%" p0-morton-index p1-morton-index)
    (loop for p0 in p0-morton-index
          for p1 in p1-morton-index
          while (eql p0 p1)
          collect p0))  
)

;; tree-space hash table

;; (defparameter *morton-level* 4)

(defun make-morton-hash-table ()
  (make-hash-table :test 'equal))

(defparameter *morton-space-hash-tabe*
  (make-morton-hash-table))

(defun hash-register-to-morton-space! (hash-table! morton-index-of-space-tree object)
  (setf (gethash morton-index-of-space-tree hash-table!) 
        (cons object (gethash morton-index-of-space-tree hash-table!) )
  ))

(defun hash-move-object! (hash-table! index-previous index-next object)
  (setf (gethash index-previous hash-table!)
        (remove object (gethash index-previous hash-table!)))
  (setf (gethash index-next hash-table!)
        (cons object (gethash index-next hash-table!)))
  )

;; morton-order-collespond to hash table

(defun objects-of-hash-index (hash-table index)
  ;; is only just index , want index xxxx
  (gethash index hash-table)
  )

(defun parents-index-list (index)
  (labels ((take (num list)
             (if (or (<= num 0) (null list))
                 nil
                 (cons (car list) (take (1- num) (cdr list))))))
    (mapcar 
     #'(lambda (n) (take n index))
     (upto 1 (length index)))))


(defun childs-index-list (index patterns)
  (mapcar
   #'(lambda (p) (append index (list p)))
   patterns))


(let ((memo (make-hash-table :test 'equal)))
  (defun progeny-index-list-aux (index level)
    (let ((childs (childs-index-list index '(0 1 2 3))))
      (cond ((<= level 0) index)
            ((= level 1) childs)
            (t (reduce #'append 
                       (cons childs
                             (mapcar #'(lambda (child) (progeny-index-list child (- level 1)))
                                     childs)))))))
  (defun progeny-index-list (index level)
    (if (not (gethash index memo))
        (setf (gethash index memo)
              (progeny-index-list-aux index level)))
    (gethash index memo)))
              

              
           

(defun index-list (index level)
  level
  (append
   
   (parents-index-list index) ;; parent and current
   (progeny-index-list index (- (+ level 1) (length index)))
   ))

(defun objects-of-morton-space-index (hash-table index)
  (let ((index-list (index-list index *morton-level*)))
    (apply #'append
           (mapcar #'(lambda (index) (objects-of-hash-index hash-table index))
                   index-list))))



;;; morton division space tree for substance-list

(defun substance-collision-detection--morton-division-tree
    (substance morton-collesponds level height width)
  (some #'(lambda (sub)
            (cond
              ((equal sub substance) nil)
              (t (incf *num-collision-detection*) ; for profile
                 (circle-collision-detection (substances-circle-parameter substance)
                                             (substances-circle-parameter sub)))))
        (objects-of-morton-space-index
         morton-collesponds
         (morton-index-of-space-tree--circle (substances-circle-parameter substance)
                                             level height width))))


(defun morton-index-of-space-tree--circle (circle-parameter level height width)
  (let* ((r (cdr (assoc 'radius circle-parameter)))
         (y (cdr (assoc 'y circle-parameter)))
         (x (cdr (assoc 'x circle-parameter))))
    (morton-index-of-space-tree (- y r) (- x r) (+ y r) (+ x r)
                                level height width)))



;; (maphash #'(lambda (key val) (format t "~A: ~A ~%" key val)) (world-morton-tree-space--substance--corresponds *sample-world*))

#|
(objects-of-morton-space-index
(world-morton-tree-space--substance--corresponds *sample-world*)
(morton-index-of-space-tree--circle (substances-circle-parameter (nth 0 (world-substances *sample-world*)))
4 *height* *width*))

(objects-of-morton-space-index
(world-morton-tree-space--substance--corresponds *sample-world*)
(morton-index-of-space-tree--circle (substances-circle-parameter (nth 0 (world-substances *sample-world*)))
4 *height* *width*))
|#



;;; binary version of morton division space tree

#|

;;(defparameter *liner-quaternary-tree*
;;  (make-array (liner-quateranry-tree-length *morton-level*)))


(defun show-binary-number (num)
  (format nil "#b~b" num))
  

(defun bit-separate32 (n)
  (let*
      (;; ___________________________  #b|xxx|xxx|xxx|xxx|xxx|xxx|xxx|xxx
       (n (logand (logior n (ash n 8)) #b00000000111111110000000011111111)) ;; #x00ff00ff))
       (n (logand (logior n (ash n 4)) #b00001111000011110000111100001111)) ;; #x0f0f0f0f))
       (n (logand (logior n (ash n 2)) #b00110011001100110011001100110011))
       (n (logand (logior n (ash n 1)) #b01010101010101010101010101010101)))
    n))

(defun get-2d-morton-number (y x)
  (logior (ash (bit-separate32 x) 0)
          (ash (bit-separate32 y) 1)))


;;; utility(?)

(defun space-key-list-to-number (key-list)
  (labels 
      ((aur (list current)
         ;; (print (car list))
         (cond ((null list) current)
               (t (aur (cdr list) (+ (car list) (* current 4)))))))
    (aur (cdr key-list) 0))) ;; aur from remove root space

|#
