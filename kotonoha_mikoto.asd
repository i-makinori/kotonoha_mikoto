
(in-package :cl-user)
(defpackage :kononoha_mikoto-asd
  (:use :cl :asdf))
(in-package :kononoha_mikoto-asd)

(defsystem "kotonoha_mikoto"
  :version "0.1.0"
  :author "i-makinori"
  :license ""
  :depends-on (:mcclim :portable-threads)
  :components
  ((:file "package")
   (:module "src"
    :components
    ((:module "utils"
      :components
      ((:file "matrix"))) ;; sequence utility
     (:module "physics"
      :components
      ((:file "space") ;; define of space
       (:file "physics") ;; substance and physics
       (:file "space-division") ;; space division
       (:file "projection-region"))) ;; camera, move-camera, select-lifes
     (:module "sekai"
      :components
      ((:file "kotonoha") ;; effect of kotonoha words
       (:file "threads")
       (:file "terrains")
       (:file "iketoshimono")
       (:file "sekai") ;; simulator
       (:file "sim_substances")
       (:file "sim_terrains")
       )) 
     (:module "mcclim"
      :components
      ((:file "mcclim") ;; define-application
       (:file "projection") ;; project world to mcclim
       (:file "gadgets") ;; gadgets. world-status, minimap, substances-status, follow-map...
       (:file "pointer-event"))) ;; mcclim's events to projection-region
     (:file "application") ;; application
     )))
  :description "")
