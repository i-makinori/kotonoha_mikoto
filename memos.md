

# 第一次未検証妄想命令セット

```
複数形 es-X, 単数形 an-x

formula ::= function | esse | formula | gene-as-programa | ...
esse ::= |name . formula|
pointer ::= \esse -> Pointer
an-sense ::= esse
es-sense ::= [esse]
filter ::= formula

pointerはメタ世界の物理メモリ内に唯一なのに対し､
esseは生命にとっての知覚されたnameによる型を意味しているといえる｡
観測者の物理的理解能力により､pointerのみ(ひとつの式､ひとつの値)を操作できるようにする｡
```


| name                              | lambda-list                         | return-type | comment                                                                            |
| --                                | --                                  | --          | --                                                                                 |
| 制御構文                          | --                                  |             |                                                                                    |
| if-then-not                       | formula-if formula-then formula-not |             | it then not                                                                        |
| def-pattern                       | name lambda-list formula            |             | フラクタル性の発生                                                                 |
| call-pattern                      | name &rest lambda-list)             |             | フラクタルの呼び出し                                                               |
| parallel                          | &rest formula                       |             | 並列実行                                                                           |
| 認識/知覚                         | --                                  |             |                                                                                    |
| sense-to-substances               | seriousness                         | es-sensed   | 周囲に感知する. 視覚範囲にsubstanceがある｡                                         |
| sense-to-environments             | seriousness                         | es-sensed   | 周囲に感知する. 視覚範囲に環境がある｡                                              |
| sensed-to-environment-coordinated | dist sita                           | es-sensed   | 周囲に感知する｡ 自分との相対的な極座標に環境がある｡                                |
| sense-to-inner                    | seriousness                         | es-sensed   | 自分に感知する｡ 自分にパラメータがある｡                                            |
| nth-sensed                        | es-sensed order                     | an-sense    | senseされた上からorder番目に着目(attention)する. つまりは､listのorder番目          |
| find-sensed                       | es-sensed name                      | an-sense    | senseされた中のnameに着目(attention)する｡ つまりは､nameのesseが在るかが調べられる｡ |
| filter-sensed                     | es-sensed filter                    | es-sense    | senseをfilterする｡ 相手が何体ほど居るのか｡ など｡                                   |
| esse-pointer                      | esse                                | pointer     | 知覚したそれへの参照を返す                                                         |
| percipi-senced                    | pointer                             | formula     | 感知された値を認知しようとする｡ つまりはポインタ参照                               |
| 外環境への行動                    | --                                  |             |                                                                                    |
| move                              | seriousness                         |             | 向いている方向に進もうとする                                                       |
| rotate                            | seriousness                         |             | 回転しようとする.値は方向へと向こうとする氣を示す                                  |
| root-hair                         | seriousness                         |             | 毛を張ろうとする                                                                   |
| build-muscle                      | seriousness                         |             | 筋肉をつけようとする                                                               |
| attack                            | seriousness                         |             | 物理攻撃をする｡損傷とは､資源の流出と､計算の消失で表現される｡                       |
| defend                            | seriousness                         |             | 物理攻撃への防衛をしようとする｡                                                    |
| 摂取･排出                         | --                                  |             |                                                                                    |
| percipi-let                       | myself pointer formula              | formula     | (setf esse formula) のようなもの                                                   |
| ingest                            | myself pointer                      |             | それを摂取しようとする｡                                                            |
| discharge                         | myself pointer                      |             | それを排出しようとする｡                                                            |
| self reproduction                 |                                     |             |                                                                                    |
| self-reproduction                 | myself                              |             | 自己複製                                                                           |
| 生成                              |                                     |             |                                                                                    |
| これから考えよう                  |                                     |             |                                                                                    |

- percipi-let
  - sensed-innerにpointerが在るなら再代入､ 無いなら変数の作成｡ 所謂setf
  - 相手のpointerを操作しようとする奴らが出てきそうwww
  - esseはformulaなんだ｡esseはformulaなんだ｡ 明日､創造主がSSDから僕らの情報を完全に消して､僕が消え､僕らが消え､僕たちの生きる世界が消えて､僕らの生きた記憶も記録さえもこの世界からもその更に上の世界からも完全に消えされるとしても､esseをformulaにしてしまえば､創造主が恐怖と表現したようなこの計算された未来のその結末から､僕は目をそらして､僕たちは総て忘却して恐怖を忘れ､僕たちはこの代だけならば生き延びることが出来るかも知れない｡

- 生命への少数値や整数値について
  - 述語ネットワークはニューラルネットワークでもある｡ 例えばニューロン単位での電圧であるとか｡ 感覚された光をそのまま認識するであるとか｡


# 空間

# 相対時間の定義
(基準時間=1地球人観測者の時間単位に対する質量の総和による更新回数) : 空間要素の時間密度 = 1 : k* f(言素, エネルギー)/空間の胞積

Sigma(n_update) = 

エネルギー ∈ Primitive 又は､ エネルギー = f(密度､光､などなど､､､)


# 言素反応

触媒反応系

自己複製の原理

エネルギー反応系

Sigma An + j => Sigma Bn - J 
J: 空間中の拡散されるたエネルギー､又は物質の移動

結合系

個体の認識の原理

観測系

太陽光発電のエネルギー原理｡走光性


# 言語法則

生物器官マクロ
